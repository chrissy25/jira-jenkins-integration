/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.com.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test case for the {@link com.marvelution.jira.plugins.jenkins.rest.SiteResource} REST resource
 * <p/>
 * Not all endpoints are checked since the {@link com.marvelution.jira.plugins.jenkins.rest.security.AdminRequired}
 * annotation is at class level
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class SiteResourceTest extends BaseRESTTest {

	public static final String SERVICE = "site";

	public SiteResourceTest(ApplicationProperties properties) {
		super(properties);
	}

	/**
	 * Validate that the Site services are allowed for admin users
	 */
	@Test
	public void testGetAllAdminUserAllowed() {
		ClientResponse response = getAdminService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(200));
	}

	/**
	 * Validate that the Site services are not allowed for anonymous users
	 */
	@Test
	public void testGetAllAnonymousNotAllowed() {
		ClientResponse response = getService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

	/**
	 * Validate that the Site services are not allowed for authenticated users
	 */
	@Test
	public void testGetAllNoAdminUserNotAllowed() {
		ClientResponse response = getUserService(SERVICE).get(ClientResponse.class);
		assertThat(response.getStatus(), is(401));
	}

}
