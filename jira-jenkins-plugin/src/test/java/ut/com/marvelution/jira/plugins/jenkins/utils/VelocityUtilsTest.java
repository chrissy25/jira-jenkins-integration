/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.com.marvelution.jira.plugins.jenkins.utils;

import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.marvelution.jira.plugins.jenkins.utils.VelocityUtils;
import org.joda.time.DateTimeConstants;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link com.marvelution.jira.plugins.jenkins.utils.VelocityUtils}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class VelocityUtilsTest {

	private VelocityUtils utils;

	@Before
	public void setup() {
		utils = new VelocityUtils(new DateTimeFormatterFactoryStub());
	}

	@Test
	public void testGetDurationString() {
		assertThat(utils.getDurationString(76L * DateTimeConstants.MILLIS_PER_SECOND), is("1m 16s"));
		assertThat(utils.getDurationString(60L * DateTimeConstants.MILLIS_PER_SECOND), is("1m"));
		assertThat(utils.getDurationString(42L * DateTimeConstants.MILLIS_PER_SECOND), is("42s"));
	}

}
