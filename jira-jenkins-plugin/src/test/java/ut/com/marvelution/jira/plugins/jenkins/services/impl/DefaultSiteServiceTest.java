/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;
import com.marvelution.jira.plugins.jenkins.dao.SiteDao;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.model.SiteType;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.services.impl.DefaultSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.URI;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Test case for the {@link com.marvelution.jira.plugins.jenkins.services.impl.DefaultSiteService}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultSiteServiceTest {

	@Mock
	private ApplicationLinkService applicationLinkService;
	@Mock
	private ApplicationLink applicationLink;
	@Mock
	private SiteDao siteDao;
	@Mock
	private JobService jobService;
	private SiteService siteService;

	@Before
	public void setUp() throws Exception {
		siteService = new DefaultSiteService(applicationLinkService, siteDao, jobService);
	}

	@Test
	public void testGetWithExistingApplicationLink() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		Site site = new Site(1);
		site.setName("Jenkins");
		site.setApplicationId(applicationId);
		when(siteDao.get(1)).thenReturn(site);
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
		when(applicationLink.getId()).thenReturn(applicationId);
		when(applicationLink.getDisplayUrl()).thenReturn(URI.create("http://localhost:8080/"));
		when(applicationLink.getType()).thenReturn(new JenkinsApplicationType(null, null));
		Site actual = siteService.get(1);
		assertThat("The site should exist", actual, notNullValue());
		assertThat(actual.getName(), is("Jenkins"));
		assertThat(actual.getApplicationId(), is(applicationId));
		assertThat(actual.getDisplayUrl(), is("http://localhost:8080/"));
		assertThat(actual.getType(), is(SiteType.JENKINS));
		verify(applicationLinkService).getApplicationLink(applicationId);
		verify(applicationLink).getDisplayUrl();
		verify(applicationLink).getType();
		verify(siteDao).get(1);
		verify(siteDao, times(0)).remove(1);
		verifyZeroInteractions(jobService);
	}

	@Test
	public void testGetWithoutExistingApplicationLink() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		Site site = new Site(1);
		site.setName("Jenkins");
		site.setApplicationId(applicationId);
		when(siteDao.get(1)).thenReturn(site);
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(null);
		assertThat(siteService.get(1), nullValue());
		verify(applicationLinkService).getApplicationLink(applicationId);
		verifyZeroInteractions(applicationLink);
		verify(siteDao).get(1);
		verify(siteDao).remove(1);
		verify(jobService).removeAllFromSite(site);
	}

	@Test
	public void testGetAll() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		Site site = new Site(1);
		site.setName("Jenkins");
		site.setApplicationId(applicationId);
		when(siteDao.getAll()).thenReturn(Lists.newArrayList(site));
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
		when(applicationLink.getId()).thenReturn(applicationId);
		when(applicationLink.getDisplayUrl()).thenReturn(URI.create("http://localhost:8080/"));
		when(applicationLink.getType()).thenReturn(new JenkinsApplicationType(null, null));
		List<Site> actual = siteService.getAll(false);
		assertThat(actual.isEmpty(), is(false));
		assertThat(actual.size(), is(1));
		assertThat("The site should exist", actual, notNullValue());
		Site first = actual.get(0);
		assertThat(first.getName(), is("Jenkins"));
		assertThat(first.getApplicationId(), is(applicationId));
		assertThat(first.getDisplayUrl(), is("http://localhost:8080/"));
		assertThat(first.getType(), is(SiteType.JENKINS));
		verify(applicationLinkService).getApplicationLink(applicationId);
		verify(applicationLink).getDisplayUrl();
		verify(applicationLink).getType();
		verify(siteDao).getAll();
		verify(siteDao, times(0)).remove(1);
		verifyZeroInteractions(jobService);
	}

	@Test
	public void testGetAllWithInvalidSites() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		Site site = new Site(1);
		site.setName("Jenkins");
		site.setApplicationId(applicationId);
		when(siteDao.getAll()).thenReturn(Lists.newArrayList(site));
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(null);
		List<Site> actual = siteService.getAll(false);
		assertThat(actual, notNullValue());
		assertThat(actual.isEmpty(), is(true));
		verify(applicationLinkService).getApplicationLink(applicationId);
		verifyZeroInteractions(applicationLink);
		verify(siteDao).getAll();
		verify(siteDao).remove(1);
		verify(jobService).removeAllFromSite(site);
	}

	@Test
	public void testGetByExistingApplicationLink() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		Site site = new Site(1);
		site.setName("Jenkins");
		site.setApplicationId(applicationId);
		when(siteDao.get(applicationId)).thenReturn(site);
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
		when(applicationLink.getId()).thenReturn(applicationId);
		when(applicationLink.getDisplayUrl()).thenReturn(URI.create("http://localhost:8080/"));
		when(applicationLink.getType()).thenReturn(new JenkinsApplicationType(null, null));
		Site actual = siteService.getByApplicationLink(applicationLink);
		assertThat("The site should exist", actual, notNullValue());
		assertThat(actual.getName(), is("Jenkins"));
		assertThat(actual.getApplicationId(), is(applicationId));
		assertThat(actual.getDisplayUrl(), is("http://localhost:8080/"));
		assertThat(actual.getType(), is(SiteType.JENKINS));
		verify(applicationLinkService, times(0)).getApplicationLink(applicationId);
		verify(applicationLink).getDisplayUrl();
		verify(applicationLink).getType();
		verify(siteDao).get(applicationId);
		verify(siteDao, times(0)).save(site);

	}

	@Test
	public void testGetByNewApplicationLink() throws Exception {
		ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("http://localhost:8080/"));
		when(siteDao.get(applicationId)).thenReturn(null);
		when(siteDao.save(any(Site.class))).thenAnswer(new Answer<Site>() {
			@Override
			public Site answer(InvocationOnMock invocation) throws Throwable {
				return (Site) invocation.getArguments()[0];
			}
		});
		when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(applicationLink);
		when(applicationLink.getId()).thenReturn(applicationId);
		when(applicationLink.getName()).thenReturn("Jenkins");
		when(applicationLink.getDisplayUrl()).thenReturn(URI.create("http://localhost:8080/"));
		when(applicationLink.getType()).thenReturn(new JenkinsApplicationType(null, null));
		Site actual = siteService.getByApplicationLink(applicationLink);
		assertThat("The site should exist", actual, notNullValue());
		assertThat(actual.getName(), is("Jenkins"));
		assertThat(actual.getApplicationId(), is(applicationId));
		assertThat(actual.getDisplayUrl(), is("http://localhost:8080/"));
		assertThat(actual.getType(), is(SiteType.JENKINS));
		verify(applicationLinkService, times(0)).getApplicationLink(applicationId);
		verify(applicationLink).getDisplayUrl();
		verify(applicationLink).getType();
		verify(siteDao).get(applicationId);
		verify(siteDao).save(any(Site.class));
	}

}
