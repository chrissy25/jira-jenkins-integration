/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.com.marvelution.jira.plugins.jenkins.services.impl;

import java.net.URI;
import java.util.List;

import com.marvelution.jira.plugins.jenkins.model.Artifact;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.ChangeSet;
import com.marvelution.jira.plugins.jenkins.model.Culprit;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.impl.RestCommunicator;
import com.marvelution.jira.plugins.jenkins.services.impl.RestResponse;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.testresources.net.MockResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * JUnit testcase for {@link com.marvelution.jira.plugins.jenkins.services.impl.RestCommunicator}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RestCommunicatorTest {

    private final URI uri = URI.create("http://ci.jenkins-ci.org");
	private final ApplicationId applicationId = ApplicationIdUtil.generate(uri);
	@Mock
	private HostApplication hostApplication;
	@Mock
	private ApplicationLink applicationLink;
	@Mock
	private ApplicationLinkRequestFactory linkRequestFactory;
	@Mock
	private ApplicationLinkRequest linkRequest;
	@Mock
	private AuthenticationConfigurationManager authenticationConfigurationManager;
	private RestCommunicator communicator;

	/**
	 * Setup the test Mocks
	 *
	 * @throws Exception in case of errors
	 */
	@Before
	public void setup() throws Exception {
		Site site = new Site(1);
		site.setApplicationId(applicationId);
		when(applicationLink.getId()).thenReturn(applicationId);
        when(applicationLink.getId()).thenReturn(applicationId);
		when(applicationLink.createNonImpersonatingAuthenticatedRequestFactory()).thenReturn(linkRequestFactory);
		when(linkRequestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(linkRequest);
		communicator = new RestCommunicator(hostApplication, site, applicationLink, authenticationConfigurationManager);
	}

	/**
	 * Test {@link RestCommunicator#getJobs()}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetJobs() throws Exception {
		setupLinkRequestAnswer("job-list.json");
		List<Job> jobs = communicator.getJobs();
		assertThat(jobs.isEmpty(), is(false));
		assertThat(jobs.size(), is(3));
		assertThat(jobs.get(0).getName(), is("jenkins_main_trunk"));
		assertThat(jobs.get(1).getName(), is("jenkins_ui-changes_branch"));
		assertThat(jobs.get(2).getName(), is("tools_maven-hpi-plugin"));
		for (Job job : jobs) {
			assertThat(job.getSiteId(), is(1));
			assertThat(job.getId(), is(0));
			assertThat(job.getBuilds().isEmpty(), is(true));
		}
		verify(applicationLink).createNonImpersonatingAuthenticatedRequestFactory();
		verify(linkRequestFactory).createRequest(Request.MethodType.GET, "/api/json");
		verify(linkRequest).execute(any(ApplicationLinkResponseHandler.class));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedJob(com.marvelution.jira.plugins.jenkins.model.Job)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedJob() throws Exception {
		setupLinkRequestAnswer("job.json");
		Job result = communicator.getDetailedJob(new Job(0, 1, "jenkins_main_trunk"));
		assertThat(result.getId(), is(0));
		assertThat(result.getSiteId(), is(1));
		assertThat(result.getName(), is("jenkins_main_trunk"));
		assertThat(result.getLastBuild(), is(0));
		assertThat(result.getDescription(), is(""));
		assertThat(result.isBuildable(), is(true));
		assertThat(result.isDeleted(), is(false));
		assertThat(result.isInQueue(), is(false));
		assertThat(result.isLinked(), is(false));
		assertThat(result.getOldestBuild(), is(1623));
		assertThat(result.getBuilds().isEmpty(), is(false));
		assertThat(result.getBuilds().size(), is(18));
		verify(applicationLink).createNonImpersonatingAuthenticatedRequestFactory();
		verify(linkRequestFactory).createRequest(Request.MethodType.GET, "/job/jenkins_main_trunk/api/json");
		verify(linkRequest).execute(any(ApplicationLinkResponseHandler.class));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(com.marvelution.jira.plugins.jenkins.model.Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuild() throws Exception {
		setupLinkRequestAnswer("build.json");
		Job job = new Job(1, 1, "jenkins_main_trunk");
		Build build = communicator.getDetailedBuild(job, 2144);
		assertThat(build.getNumber(), is(2144));
		assertThat(build.getJobId(), is(1));
		assertThat(build.getDuration(), is(135329L));
		assertThat(build.getDisplayName(), nullValue());
		assertThat(build.getBuiltOn(), is("remote-slave-6"));
		assertThat(build.getTimestamp(), is(1355405446078L));
		assertThat(build.getResult(), is("FAILURE"));
		assertThat(build.getCause(), is("Started by an SCM change"));
		assertThat(build.getArtifacts().size(), is(2));
		Artifact artifact = build.getArtifacts().get(0);
		assertThat(artifact.getFileName(), is("jenkins-core-1.495-SNAPSHOT.jar"));
		assertThat(artifact.getDisplayPath(), is("jenkins-core-1.495-SNAPSHOT.jar"));
		assertThat(artifact.getRelativePath(), is("core/target/jenkins-core-1.495-SNAPSHOT.jar"));
		artifact = build.getArtifacts().get(1);
		assertThat(artifact.getFileName(), is("jenkins.war"));
		assertThat(artifact.getDisplayPath(), is("jenkins.war"));
		assertThat(artifact.getRelativePath(), is("war/target/jenkins.war"));
		assertThat(build.getCulprits().size(), is(2));
		Culprit culprit = build.getCulprits().get(0);
		assertThat(culprit.getUsername(), is("Vojtech Juranek"));
		assertThat(culprit.getFullName(), is("Vojtech Juranek"));
		culprit = build.getCulprits().get(1);
		assertThat(culprit.getUsername(), is("kohsuke"));
		assertThat(culprit.getFullName(), is("Kohsuke Kawaguchi"));
		assertThat(build.getChangeSet().size(), is(2));
		ChangeSet changeSet = build.getChangeSet().get(0);
		assertThat(changeSet.getcommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Add alternatives (used e.g. for openjdk7) to java candidates\n"));
		changeSet = build.getChangeSet().get(1);
		assertThat(changeSet.getcommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Add alternatives (used e.g. for openjdk7) to java candidates\n"));
		assertThat(build.getTestResults(), notNullValue());
		assertThat(build.getTestResults().getFailed(), is(0));
		assertThat(build.getTestResults().getSkipped(), is(0));
		assertThat(build.getTestResults().getTotal(), is(3121));
		verify(applicationLink).createNonImpersonatingAuthenticatedRequestFactory();
		verify(linkRequestFactory).createRequest(Request.MethodType.GET,
				"/job/jenkins_main_trunk/2144/api/json?depth=0");
		verify(linkRequest).execute(any(ApplicationLinkResponseHandler.class));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(com.marvelution.jira.plugins.jenkins.model.Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuildWithDisplayName() throws Exception {
		setupLinkRequestAnswer("build-with-display-name.json");
		Job job = new Job(1, 1, "jenkins_main_trunk");
		Build build = communicator.getDetailedBuild(job, 2144);
		assertThat(build.getNumber(), is(2144));
		assertThat(build.getJobId(), is(1));
		assertThat(build.getDuration(), is(135329L));
		assertThat(build.getDisplayName(), is("#1.55.2144"));
		assertThat(build.getBuiltOn(), is("remote-slave-6"));
		assertThat(build.getTimestamp(), is(1355405446078L));
		assertThat(build.getResult(), is("FAILURE"));
		assertThat(build.getCause(), is("Started by an SCM change"));
		assertThat(build.getArtifacts().size(), is(2));
		Artifact artifact = build.getArtifacts().get(0);
		assertThat(artifact.getFileName(), is("jenkins-core-1.495-SNAPSHOT.jar"));
		assertThat(artifact.getDisplayPath(), is("jenkins-core-1.495-SNAPSHOT.jar"));
		assertThat(artifact.getRelativePath(), is("core/target/jenkins-core-1.495-SNAPSHOT.jar"));
		artifact = build.getArtifacts().get(1);
		assertThat(artifact.getFileName(), is("jenkins.war"));
		assertThat(artifact.getDisplayPath(), is("jenkins.war"));
		assertThat(artifact.getRelativePath(), is("war/target/jenkins.war"));
		assertThat(build.getCulprits().size(), is(2));
		Culprit culprit = build.getCulprits().get(0);
		assertThat(culprit.getUsername(), is("Vojtech Juranek"));
		assertThat(culprit.getFullName(), is("Vojtech Juranek"));
		culprit = build.getCulprits().get(1);
		assertThat(culprit.getUsername(), is("kohsuke"));
		assertThat(culprit.getFullName(), is("Kohsuke Kawaguchi"));
		assertThat(build.getChangeSet().size(), is(2));
		ChangeSet changeSet = build.getChangeSet().get(0);
		assertThat(changeSet.getcommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Add alternatives (used e.g. for openjdk7) to java candidates\n"));
		changeSet = build.getChangeSet().get(1);
		assertThat(changeSet.getcommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Add alternatives (used e.g. for openjdk7) to java candidates\n"));
		assertThat(build.getTestResults(), notNullValue());
		assertThat(build.getTestResults().getFailed(), is(0));
		assertThat(build.getTestResults().getSkipped(), is(0));
		assertThat(build.getTestResults().getTotal(), is(3121));
		verify(applicationLink).createNonImpersonatingAuthenticatedRequestFactory();
		verify(linkRequestFactory).createRequest(Request.MethodType.GET,
				"/job/jenkins_main_trunk/2144/api/json?depth=0");
		verify(linkRequest).execute(any(ApplicationLinkResponseHandler.class));
	}

	/**
	 * Helper method to setup the {@link Answer} for the {@link #linkRequest}
	 *
	 * @param json the JSON resource name to setup as the response
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	private void setupLinkRequestAnswer(final String json) throws Exception {
		when(linkRequest.execute(any(ApplicationLinkResponseHandler.class))).thenAnswer(new Answer<RestResponse>() {
			@Override
			public RestResponse answer(InvocationOnMock invocation) throws Throwable {
				ApplicationLinkResponseHandler<RestResponse> handler = (ApplicationLinkResponseHandler<RestResponse>)
						invocation.getArguments()[0];
				MockResponse response = new MockResponse();
				response.setStatusCode(200);
				response.setStatusText("I'm oke");
				response.setSuccessful(true);
				response.setResponseBodyAsStream(getClass().getResourceAsStream(json));
				return handler.handle(response);
			}
		});
	}

}
