/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.spi.application.NonAppLinksEntityType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.util.Assertions;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import com.marvelution.jira.plugins.jenkins.utils.URIUtils;

import java.net.URI;

/**
 * Hudson Entity Link Type
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class HudsonJobEntityLinkType extends IconizedIdentifiableType implements NonAppLinksEntityType {

	private static final TypeId TYPE_ID = new TypeId("hudson.job");

	public HudsonJobEntityLinkType(JenkinsPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
		super(pluginUtil, webResourceUrlProvider);
	}

	@Override
	public TypeId getId() {
		return TYPE_ID;
	}

	@Override
	public Class<? extends ApplicationType> getApplicationType() {
		return HudsonApplicationType.class;
	}

	@Override
	public String getI18nKey() {
		return "hudson.job";
	}

	@Override
	public String getPluralizedI18nKey() {
		return "hudson.jobs";
	}

	@Override
	public String getShortenedI18nKey() {
		return "job";
	}

	@Override
	public URI getDisplayUrl(ApplicationLink applicationLink, String job) {
		Assertions.isTrue(String.format("Application link %s is not of type %s", applicationLink.getId(),
				getApplicationType().getName()), (applicationLink.getType() instanceof HudsonApplicationType));
		return URIUtils.appendPathsToURI(applicationLink.getDisplayUrl(), "job", job);
	}

	@Override
	public String toString() {
		return "Hudson Job Type";
	}

}
