/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.google.common.collect.Maps;
import com.marvelution.jira.plugins.jenkins.dao.IssueDao;
import com.marvelution.jira.plugins.jenkins.dao.JobDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Progress;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.sync.Synchronizer;
import com.marvelution.jira.plugins.jenkins.sync.impl.DefaultSynchronizationOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Default {@link com.marvelution.jira.plugins.jenkins.services.JobService} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultJobService implements JobService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJobService.class);
	private final JobDao jobDao;
	private final IssueDao issueDao;
	private final BuildService buildService;
	private final Synchronizer synchronizer;
	private final CommunicatorFactory communicatorFactory;

	public DefaultJobService(JobDao jobDao, IssueDao issueDao, BuildService buildService, Synchronizer synchronizer,
	                         CommunicatorFactory communicatorFactory) {
		this.jobDao = jobDao;
		this.issueDao = issueDao;
		this.buildService = buildService;
		this.synchronizer = synchronizer;
		this.communicatorFactory = communicatorFactory;
	}

	@Override
	public synchronized void syncJobList(Site site) {
		LOGGER.info("Synchronizing all Jobs from {}", site.getName());
		Communicator communicator = communicatorFactory.get(site);
		try {
			if (communicator.isRemoteOnlineAndAccessible()) {
				Map<String, Job> localJobs = Maps.newHashMap();
				for (Job job : jobDao.getAllBySiteId(site.getId(), true)) {
					localJobs.put(job.getName(), job);
				}
				Map<String, Job> remoteJobs = Maps.newHashMap();
				for (Job job : communicator.getJobs()) {
					remoteJobs.put(job.getName(), job);
					if (localJobs.containsKey(job.getName())) {
						// Update the existing Job, but not the last build number as this is set by the Synchronizer!
						Job localJob = localJobs.get(job.getName());
						LOGGER.info("Updating Job {} from {}", localJob.getName(), site.getName());
						localJob.setDeleted(false);
						localJob.setName(job.getName());
						jobDao.save(localJob);
					} else {
						LOGGER.info("Adding new Job {} from {}", job.getName(), site.getName());
						job.setSiteId(site.getId());
						job.setDeleted(false);
						job.setLinked(site.isAutoLink());
						job.setLastBuild(0);
						jobDao.save(job);
					}
				}
				for (Map.Entry<String, Job> entry : localJobs.entrySet()) {
					if (!remoteJobs.containsKey(entry.getKey())) {
						// The local job is no longer on the remote site so mark it as deleted
						LOGGER.info("Job {} is no longer on {} marking it as deleted", entry.getValue().getName(),
								site.getName());
						jobDao.delete(entry.getValue());
					}
				}
				syncAllFromSite(site);
			} else {
				LOGGER.info("Skipping job list synchronisation of {}, remote is not online", site.getName());
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Skipping job list synchronisation of {}, remote is online but not accessible", site.getName());
		}
	}

	@Override
	public void sync(int jobId) {
		Job job = jobDao.get(jobId);
		if (job != null) {
			doSync(job);
		}
	}

	@Override
	public void syncAllFromSite(Site site) {
		for (Job job : getAllBySite(site)) {
			doSync(job);
		}
	}

	@Override
	public Progress getSyncStatus(Job job) {
		return synchronizer.getProgress(job);
	}

	/**
	 * Trigger the synchronization of the given {@link Job}
	 *
	 * @param job the {@link Job} to synchronize
	 */
	private void doSync(Job job) {
		if (job.isLinked()) {
			synchronizer.synchronize(job, new DefaultSynchronizationOperation(this, buildService,
					communicatorFactory.get(job.getSiteId()), job));
		}
	}

	@Override
	public List<Job> getAllBySite(Site site) {
		return jobDao.getAllBySiteId(site.getId(), false);
	}

	@Override
	public List<Job> getAllBySite(Site site, boolean includeDeleted) {
		return jobDao.getAllBySiteId(site.getId(), includeDeleted);
	}

	@Override
	public List<Job> getAll() {
		return jobDao.getAll(false);
	}

	@Override
	public List<Job> getAll(boolean includeDeleted) {
		return jobDao.getAll(includeDeleted);
	}

	@Override
	public Job get(int jobId) {
		return jobDao.get(jobId);
	}

	@Override
	public List<Job> get(String name) {
		return jobDao.get(name);
	}

	@Override
	public URI getJobUrl(Job job) {
		return getCommunicatorForJob(job).getJobUrl(job);
	}

	@Override
	public URI getJobBuildUrl(Job job, Build build) {
		return getCommunicatorForJob(job).getBuildUrl(job, build);
	}

	@Override
	public void enable(int jobId, boolean enabled) {
		Job job = jobDao.get(jobId);
		if (job != null) {
			if (!enabled) {
				synchronizer.stopSynchronization(job);
			}
			job.setLinked(enabled);
			jobDao.save(job);
		}
	}

	@Override
	public Job save(Job job) {
		return jobDao.save(job);
	}

	@Override
	public void remove(Job job) {
		buildService.removeAllInJob(job);
		jobDao.remove(job.getId());
	}

	@Override
	public void removeAllFromSite(Site site) {
		for (Job job : getAllBySite(site)) {
			buildService.removeAllInJob(job);
		}
		jobDao.removeAllBySiteId(site.getId());
	}

	@Override
	public void delete(Job job) {
		jobDao.delete(job);
		buildService.deleteAllInJob(job);
	}

	@Override
	public void deleteAllFromSite(Site site) {
		for (Job job : getAllBySite(site)) {
			delete(job);
		}
	}

	@Override
	public Iterable<String> getRelatedIssuesKeys(Job job) {
		return issueDao.getIssueKeysByJob(job);
	}

	/**
	 * Get a {@link Communicator} for the given {@link Job}
	 *
	 * @param job the {@link Job} to get a
	 * @return
	 */
	private Communicator getCommunicatorForJob(Job job) {
		return communicatorFactory.get(job.getSiteId());
	}

}
