/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.listeners;

import com.atlassian.applinks.api.event.ApplicationLinkAuthConfigChangedEvent;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ApplicationLinksIDChangedEvent} listener
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsApplicationLinkChangedListener extends AbstractJenkinsApplicationLinkListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsApplicationLinkChangedListener.class);
	private final SiteService siteService;
	private final JobService jobService;
	private final CommunicatorFactory communicatorFactory;

	public JenkinsApplicationLinkChangedListener(EventPublisher eventPublisher, SiteService siteService,
	                                             JobService jobService, CommunicatorFactory communicatorFactory) {
		super(eventPublisher);
		this.siteService = siteService;
		this.jobService = jobService;
		this.communicatorFactory = communicatorFactory;
	}

	/**
	 * {@link EventListener} for {@link ApplicationLinksIDChangedEvent} events
	 *
	 * @param event the {@link ApplicationLinksIDChangedEvent} event
	 */
	@EventListener
	public void onApplicationLinkIDChanged(ApplicationLinksIDChangedEvent event) {
		if (supportsApplicationLinkType(event)) {
			LOGGER.info("Updating applinkId from {} to {}", event.getOldApplicationId().get(),
					event.getApplicationId().get());
			siteService.updateApplicationLink(event.getApplicationLink(), event.getOldApplicationId());
		} else {
			LOGGER.debug("Skipping applinks event for type {}", event.getApplicationType().getI18nKey());
		}
	}

	/**
	 * {@link EventListener} for {@link ApplicationLinkAuthConfigChangedEvent} events
	 *
	 * @param event the {@link ApplicationLinkAuthConfigChangedEvent} event
	 */
	@EventListener
	public void onApplicationLinkAuthConfigChangedEvent(ApplicationLinkAuthConfigChangedEvent event) {
		if (supportsApplicationLinkType(event)) {
			Site site = siteService.getByApplicationLink(event.getApplicationLink());
			LOGGER.info("Auth details changed for {}, scheduling job synchronisation", site.getName());
			site.setSupportsBackLink(communicatorFactory.get(site).isJenkinsPluginInstalled());
			siteService.save(site);
			jobService.syncJobList(site);
		} else {
			LOGGER.debug("Skipping applinks event for type {}", event.getApplicationType().getI18nKey());
		}
	}

}
