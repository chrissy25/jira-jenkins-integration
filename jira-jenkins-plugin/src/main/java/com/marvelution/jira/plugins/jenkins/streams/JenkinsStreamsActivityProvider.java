/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.streams;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.*;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.*;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Culprit;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import com.marvelution.jira.plugins.jenkins.utils.VelocityUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * {@link StreamsActivityProvider} for Jenkins Builds
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsStreamsActivityProvider implements StreamsActivityProvider {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsStreamsActivityProvider.class);
	private final ProjectManager projectManager;
	private final PermissionManager permissionManager;
	private final JiraAuthenticationContext authenticationContext;
	private final I18nResolver i18nResolver;
	private final SiteService siteService;
	private final JobService jobService;
	private final BuildService buildService;
	private final UserProfileAccessor userProfileAccessor;
	private final TemplateRenderer templateRenderer;
	private final JenkinsPluginUtil pluginUtil;
	private final WebResourceUrlProvider webResourceUrlProvider;
	private final VelocityUtils velocityUtils;
	private final Function<String, Project> keyToProject = new Function<String, Project>() {
		@Override
		public Project apply(@Nullable String from) {
			return projectManager.getProjectObjByKey(from);
		}
	};
	private final Function<Project, String> projectToKey = new Function<Project, String>() {
		@Override
		public String apply(Project from) {
			return from.getKey();
		}
	};
	private final Predicate<Project> hasViewSourcePermission = new Predicate<Project>() {
		@Override
		public boolean apply(@Nullable Project input) {
			return input != null && permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, input,
					authenticationContext.getLoggedInUser());
		}
	};

	public JenkinsStreamsActivityProvider(ProjectManager projectManager, PermissionManager permissionManager,
	                                      JiraAuthenticationContext authenticationContext, I18nResolver i18nResolver,
	                                      SiteService siteService, BuildService buildService, JobService jobService,
	                                      UserProfileAccessor userProfileAccessor, TemplateRenderer templateRenderer,
	                                      JenkinsPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider,
	                                      VelocityUtils velocityUtils) {
		this.projectManager = projectManager;
		this.permissionManager = permissionManager;
		this.authenticationContext = authenticationContext;
		this.i18nResolver = i18nResolver;
		this.siteService = siteService;
		this.buildService = buildService;
		this.jobService = jobService;
		this.userProfileAccessor = userProfileAccessor;
		this.templateRenderer = templateRenderer;
		this.pluginUtil = pluginUtil;
		this.webResourceUrlProvider = webResourceUrlProvider;
		this.velocityUtils = velocityUtils;
	}

	@Override
	public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest activityRequest) {
		final BuildIssueFilter filter = new BuildIssueFilter();
		filter.setInProjectKeys(getInProjectsByPermission(Filters.getIsValues(activityRequest.getStandardFilters().get
				(StandardStreamsFilterOption.PROJECT_KEY))));
		filter.setNotInProjectKeys(Filters.getNotValues(activityRequest.getStandardFilters().get
				(StandardStreamsFilterOption.PROJECT_KEY)));
		filter.setInIssueKeys(Filters.getIsValues(activityRequest.getStandardFilters().get(StandardStreamsFilterOption
				.ISSUE_KEY.getKey())));
		filter.setNotInIssueKeys(Filters.getNotValues(activityRequest.getStandardFilters().get
				(StandardStreamsFilterOption.ISSUE_KEY.getKey())));
		filter.setInUsers(Filters.getIsValues(activityRequest.getStandardFilters().get(StandardStreamsFilterOption.USER
				.getKey())));
		filter.setNotInUsers(Filters.getNotValues(activityRequest.getStandardFilters().get(StandardStreamsFilterOption
				.USER.getKey())));

		LOGGER.debug("Streams filter: " + filter);
		return new CancellableTask<StreamsFeed>() {
			private final AtomicBoolean cancelled = new AtomicBoolean(false);

			@Override
			public StreamsFeed call() throws Exception {
				Iterable<StreamsEntry> entries = Lists.newArrayList();
				if (!filter.getInProjectKeys().isEmpty() && filter.getInUsers().isEmpty()) {
					Iterable<Build> builds = buildService.getLatestBuildsByFilter(activityRequest.getMaxResults(),
							filter);
					if (cancelled.get()) {
						throw new CancelledException();
					}
					LOGGER.debug("Found build entries for stream: " + builds);
					entries = transformEntries(builds, cancelled);
				}
				return new StreamsFeed(i18nResolver.getText("jenkins.streams.feed.title"), entries,
						Option.<String>none());
			}

			@Override
			public CancellableTask.Result cancel() {
				cancelled.set(true);
				return CancellableTask.Result.CANCELLED;
			}
		};
	}

	/**
	 * Transform the given Builds to StreamEntries
	 *
	 * @param builds    the Builds to transform
	 * @param cancelled cancelled flag
	 * @return the stream entries
	 */
	private Iterable<StreamsEntry> transformEntries(Iterable<Build> builds, AtomicBoolean cancelled) {
		Set<Build> processed = Sets.newHashSet();
		List<StreamsEntry> entries = Lists.newArrayList();
		for (Build build : builds) {
			if (cancelled.get()) {
				throw new CancelledException();
			}
			if (!processed.contains(build)) {
				try {
					entries.add(toStreamEntry(build));
				} catch (Exception e) {
					LOGGER.warn("Unable to get Streams Entry for build {}: {}", build, e.getMessage());
					LOGGER.debug("toStreamEntry error", e);
				}
				processed.add(build);
			}
		}
		return entries;
	}

	/**
	 * Transform a {@link Build} to a {@link StreamsEntry}
	 *
	 * @param build the {@link Build} to transform
	 * @return the {@link StreamsEntry}
	 */
	private StreamsEntry toStreamEntry(final Build build) {
		final Job job = jobService.get(build.getJobId());
		final Site site = siteService.get(job.getSiteId());
		final URI buildUrl = jobService.getJobBuildUrl(job, build);
		final ApplicationLink link = siteService.getApplicationLink(site);
		StreamsEntry.ActivityObject activityObject = new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject
				.params().id(String.valueOf(build.getId())).alternateLinkUri(buildUrl).activityObjectType
						(ActivityObjectTypes.status()));
		StreamsEntry.Renderer renderer = new StreamsEntry.Renderer() {

			@Override
			public Html renderTitleAsHtml(StreamsEntry streamsEntry) {
				Map<String, Object> context = Maps.newHashMap();
				String displayName;
				if (StringUtils.isBlank((displayName = build.getDisplayName()))) {
					displayName = job.getDisplayName() + " > #" + build.getNumber();
				}
				context.put("build_display_name", displayName);
				context.put("build_url", buildUrl);
				context.put("build_result", build.getResult());
				return new Html(renderTemplate("/templates/stream-title.vm", context));
			}

			@Override
			public Option<Html> renderContentAsHtml(StreamsEntry streamsEntry) {
				Map<String, Object> context = Maps.newHashMap();
				context.put("build_duration", velocityUtils.getDurationString(build.getDuration()));
				context.put("build_cause", build.getCause());
				context.put("node", build.getBuiltOn());
				return Option.some(new Html(renderTemplate("/templates/stream-content.vm", context)));
			}

			@Override
			public Option<Html> renderSummaryAsHtml(StreamsEntry streamsEntry) {
				return Option.none();
			}

		};
		UserProfile first = null;
		List<UserProfile> others = Lists.newArrayList();
		if (build.getCulprits().isEmpty()) {
			first = new UserProfile.Builder(site.getName()).profilePictureUri(Option.option(URI.create(this
					.webResourceUrlProvider.getStaticPluginResourceUrl(pluginUtil.getPluginKey() +
							":jenkins-images", "images", UrlMode.ABSOLUTE) + "/icon96_" + link.getType().getI18nKey() + ".png")))
					.profilePageUri(Option.option(link.getDisplayUrl())).build();
		} else {
			for (Culprit culprit : build.getCulprits()) {
				if (first == null) {
					first = userProfileAccessor.getUserProfile(culprit.getUsername());
				} else {
					others.add(userProfileAccessor.getUserProfile(culprit.getUsername()));
				}
			}
		}
		return new StreamsEntry(StreamsEntry.params()
				.id(buildUrl)
				.postedDate(new DateTime(build.getTimestamp()))
				.authors(ImmutableNonEmptyList.of(first, others))
				.addActivityObject(activityObject)
				.verb(ActivityVerbs.post())
				.alternateLinkUri(buildUrl)
				.renderer(renderer)
				.applicationType("com.marvelution.jenkins"), i18nResolver);
	}

	/**
	 * Get all the Project keys where the user has view source permissions for
	 *
	 * @param inProjectKeys {@link Set} of project keys set in the gadget
	 * @return the {@link Set} of projects to include in the filter
	 */
	private Set<String> getInProjectsByPermission(Set<String> inProjectKeys) {
		Iterable<Project> projects;
		if (inProjectKeys.isEmpty()) {
			projects = projectManager.getProjectObjects();
		} else {
			projects = Iterables.transform(inProjectKeys, keyToProject);
		}
		return Sets.newHashSet(Iterables.transform(Iterables.filter(projects, hasViewSourcePermission), projectToKey));
	}

	/**
	 * Render the given template using the given context
	 *
	 * @param template the template name
	 * @param context  the context {@link Map}
	 * @return the rendered template
	 */
	private String renderTemplate(String template, Map<String, Object> context) {
		StringWriter writer = new StringWriter();
		try {
			templateRenderer.render(template, context, writer);
		} catch (IOException e) {
			LOGGER.warn(e.getMessage());
			LOGGER.debug("Failed to render template: " + template, e);
		}
		return writer.toString();
	}

}
