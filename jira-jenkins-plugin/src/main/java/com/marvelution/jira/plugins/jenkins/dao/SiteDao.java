/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao;

import com.atlassian.applinks.api.ApplicationId;
import com.marvelution.jira.plugins.jenkins.model.Site;

import java.util.List;

/**
 * {@link com.marvelution.jira.plugins.jenkins.ao.SiteMapping} data access service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SiteDao {

	/**
	 * Get the {@link Site} by its Id
	 *
	 * @param siteId the Id of the {@link Site} to get
	 * @return the {@link Site}
	 */
	Site get(int siteId);

	/**
	 * Get the {@link Site} by its {@link ApplicationId}
	 *
	 * @param applicationId the {@link ApplicationId} of the {@link Site}
	 * @return the {@link Site}
	 */
	Site get(ApplicationId applicationId);

	/**
	 * Get all the {@link Site}s
	 *
	 * @return all the {@link Site}s
	 */
	List<Site> getAll();

	/**
	 * Save the given {@link Site}
	 *
	 * @param site the {@link Site} to save
	 * @return the saved {@link Site}
	 */
	Site save(Site site);

	/**
	 * Remove the {@link Site} by its Id
	 *
	 * @param siteId the Id of the {@link Site} to remove
	 */
	void remove(int siteId);

}
