/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.ao.JobMapping;
import com.marvelution.jira.plugins.jenkins.ao.SiteMapping;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonJobEntityLinkType;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsJobEntityLinkType;
import com.marvelution.jira.plugins.jenkins.dao.JobDao;
import com.marvelution.jira.plugins.jenkins.model.Job;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.marvelution.jira.plugins.jenkins.ao.JobMapping.*;

/**
 * The Default implementation of the {@link JobDao} interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultJobDao implements JobDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJobDao.class);
	private final ActiveObjects ao;
	private final MutatingEntityLinkService entityLinkService;
	private final Function<JobMapping, Job> jobMappingToJobFunction = new Function<JobMapping, Job>() {
		@Override
		public Job apply(@Nullable JobMapping from) {
			if (from == null) {
				return null;
			}
			Job job = new Job(from.getID(), from.getSiteId(), from.getName());
			job.setDisplayName(from.getDisplayName());
			job.setLinked(from.isLinked());
			job.setLastBuild(from.getLastBuild());
			return job;
		}
	};

	public DefaultJobDao(ActiveObjects ao, MutatingEntityLinkService mutatingEntityLinkService) {
		this.ao = ao;
		this.entityLinkService = mutatingEntityLinkService;
	}

	@Override
	public List<Job> getAllBySiteId(final int siteId, final boolean includeDeleted) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				Query query = Query.select();
				if (includeDeleted) {
					query.where(SITE_ID + " = ?", siteId);
				} else {
					query.where(SITE_ID + " = ? AND " + DELETED + " = ?", siteId, Boolean.FALSE);
				}
				query.order(NAME);
				return Arrays.asList(ao.find(JobMapping.class, query));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

	@Override
	public List<Job> getAll(final boolean includeDeleted) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				Query query = Query.select();
				if (!includeDeleted) {
					query.where(DELETED + " = ?", Boolean.FALSE);
				}
				query.order(NAME);
				return Arrays.asList(ao.find(JobMapping.class, query));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

	@Override
	public Job save(final Job job) {
		LOGGER.debug("Saving {}", job.toString());
		JobMapping mapping = ao.executeInTransaction(new TransactionCallback<JobMapping>() {
			@Override
			public JobMapping doInTransaction() {
				JobMapping mapping;
				if (job.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(SITE_ID, job.getSiteId());
					map.put(NAME, job.getName());
					map.put(DISPLAY_NAME, job.getDisplayNameOrNull());
					map.put(LAST_BUILD, job.getLastBuild());
					map.put(LINKED, job.isLinked());
					map.put(DELETED, job.isDeleted());
					mapping = ao.create(JobMapping.class, map);
					mapping = ao.find(JobMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(JobMapping.class, job.getId());
					mapping.setSiteId(job.getSiteId());
					mapping.setName(job.getName());
					mapping.setDisplayName(job.getDisplayNameOrNull());
					mapping.setLastBuild(job.getLastBuild());
					mapping.setLinked(job.isLinked());
					mapping.setDeleted(job.isDeleted());
					mapping.save();
				}
				return mapping;
			}
		});
		return jobMappingToJobFunction.apply(mapping);
	}

	@Override
	public void remove(int jobId) {
		ao.delete(ao.get(JobMapping.class, jobId));
	}

	@Override
	public void removeAllBySiteId(final int siteId) {
		ao.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				JobMapping[] mappings = ao.find(JobMapping.class, Query.select().where(SITE_ID + " = ?",
						siteId));
				ao.delete(mappings);
				return null;
			}
		});
	}

	@Override
	public void delete(Job job) {
		job.setDeleted(true);
		save(job);
	}

	@Override
	public Job get(final int jobId) {
		JobMapping mapping = ao.executeInTransaction(new TransactionCallback<JobMapping>() {
			@Override
			public JobMapping doInTransaction() {
				return ao.get(JobMapping.class, jobId);
			}
		});
		return jobMappingToJobFunction.apply(mapping);
	}

	@Override
	public List<Job> get(final String name) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				return Arrays.asList(ao.find(JobMapping.class, Query.select().where(NAME + " = ?", name)));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

	@Override
	public List<Job> getLinked(String projectKey) {
		final StringBuilder where = new StringBuilder();
		for (EntityLink entityLink : entityLinkService.getEntityLinksForKey(projectKey, JiraProjectEntityType.class)) {
			if (entityLink.getType() instanceof JenkinsJobEntityLinkType || entityLink.getType() instanceof HudsonJobEntityLinkType) {
				if (where.length() > 0) {
					where.append(" OR ");
				}
				where.append("(site.").append(SiteMapping.APPLINK_ID).append(" = '").append(entityLink.getApplicationLink().getId().get())
						.append("' AND job.").append(NAME).append(" = '").append(entityLink.getKey()).append("')");
			}
		}
		if (where.length() > 0) {
			JobMapping[] mappings = ao.executeInTransaction(new TransactionCallback<JobMapping[]>() {
				@Override
				public JobMapping[] doInTransaction() {
					return ao.find(JobMapping.class, Query.select()
							.alias(JobMapping.class, "job")
							.alias(SiteMapping.class, "site")
							.from(JobMapping.class)
							.join(SiteMapping.class, "site.ID = job." + SITE_ID)
							.where(where.toString()));
				}
			});
			LOGGER.debug("Project {} is linked to {} jobs via entity links. (where: {})",
					new Object[] { projectKey, mappings.length, where });
			return Lists.transform(Arrays.asList(mappings), jobMappingToJobFunction);
		} else {
			return Lists.newArrayList();
		}
	}

}
