/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.Set;
import javax.annotation.Nullable;

import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.ChangeSet;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Progress;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import com.marvelution.jira.plugins.jenkins.sync.SynchronizationOperationException;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugins.dvcs.sync.impl.IssueKeyExtractor;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default {@link SynchronizationOperation}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultSynchronizationOperation implements SynchronizationOperation {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationOperation.class);
	public static final Ordering<Build> BUILD_ORDERING = new Ordering<Build>() {

		@Override
		public int compare(@Nullable Build build, @Nullable Build build2) {
			return Ints.compare(build != null ? build.getNumber() : 0, build2 != null ? build2.getNumber() : 0);
		}

	};
	private final JobService jobService;
	private final BuildService buildService;
	private final Communicator communicator;
	private final Progress progress;
	private Job job;

	public DefaultSynchronizationOperation(JobService jobService, BuildService buildService,
	                                       Communicator communicator, Job job) {
		this.jobService = jobService;
		this.buildService = buildService;
		this.communicator = communicator;
		this.job = job;
		progress = new Progress();
	}

	@Override
	public void synchronise() throws SynchronizationOperationException {
		int buildCount = 0, issueCount = 0, errorCount = 0;
		try {
			if (communicator.isRemoteOnlineAndAccessible()) {
				communicator.registerBuildNotifier(job);
				try {
					LOGGER.debug("Synchronizing builds from Job {}", job.getName());
					job = communicator.getDetailedJob(job);
					for (Build build : getBuildsToSync(job)) {
						if (progress.isShouldStop()) {
							break;
						}
						try {
							Build detailedBuild = communicator.getDetailedBuild(job, build);
							if (StringUtils.isNotBlank(detailedBuild.getResult())) {
								if (build.getNumber() > job.getLastBuild()) {
									job.setLastBuild(build.getNumber());
								}
								buildCount++;
								LOGGER.debug("Synchronizing Build {} from {}", detailedBuild.getNumber(), job.getName());
								detailedBuild = buildService.save(detailedBuild);
								for (String key : extractIssueKeys(detailedBuild)) {
									key = key.toUpperCase();
									try {
										if (buildService.link(detailedBuild, key)) {
											issueCount++;
										}
									} catch (Exception e) {
										if (progress.isShouldStop()) {
											break;
										}
										LOGGER.warn("Failed to link build {} of {} with issue {}",
										            new Object[] { build.getNumber(), job.getName(), key });
									}
								}
							} else {
								LOGGER.debug("Skipping Build {} from {}", detailedBuild.getNumber(), job.getName());
								break;
							}
						} catch (Exception e) {
							LOGGER.warn("Failed to synchronize build {} of {}: {}", new Object[] { build.getNumber(),
							                                                                       job.getName(), e.getMessage(), e });
							errorCount++;
						}
						progress.updateProgress(buildCount, issueCount, errorCount);
					}
					LOGGER.info("Marking all builds of {} prior to {} as deleted", job.getName(), job.getOldestBuild());
					buildService.deleteAllInJob(job, job.getOldestBuild());
					jobService.save(job);
				} catch (Exception e) {
					LOGGER.warn("Failed to Synchronize {}: {}", new Object[] { job.getName(), e.getMessage(), e });
				}
			} else {
				LOGGER.info("Skipping synchronisation of {}, remote is not online", job.getName());
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Skipping synchronisation of {}, remote is online but not accessible", job.getName());
		}
	}

	@Override
	public Progress getProgress() {
		return progress;
	}

	/**
	 * Get all the remote builds from the Job given starting from the last build given
	 *
	 * @param job the {@link Job} to get the builds for
	 * @return collection of {@link Build} objects
	 */
	private Iterable<Build> getBuildsToSync(final Job job) {
		return BUILD_ORDERING.sortedCopy(Iterables.filter(job.getBuilds(), new Predicate<Build>() {
			@Override
			public boolean apply(@Nullable Build build) {
				return build != null && build.getNumber() > job.getLastBuild();
			}
		}));
	}

	/**
	 * Extract all the issue keys from the given {@link Build} It will look for issue keys in the build cause,
	 * change-sets and artifacts
	 *
	 * @param build the {@link Build} to extract the issue keys from
	 * @return the {@link Set} of extracted issue keys
	 */
	private Set<String> extractIssueKeys(Build build) {
		Set<String> keys = Sets.newHashSet();
		keys.addAll(IssueKeyExtractor.extractIssueKeys(build.getCause()));
		for (ChangeSet changeSet : build.getChangeSet()) {
			keys.addAll(IssueKeyExtractor.extractIssueKeys(changeSet.getMessage()));
		}
		LOGGER.debug("Found [{}] related to {}", StringUtils.join(keys, ", "), build.toString());
		return keys;
	}

}
