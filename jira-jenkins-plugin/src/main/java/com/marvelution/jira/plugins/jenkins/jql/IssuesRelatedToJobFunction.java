/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * JQL Function to located issues related to a Jenkins Job
 *
 * @author Mark Rekveld
 * @since 1.2.1
 */
public class IssuesRelatedToJobFunction extends AbstractJqlFunction {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuesRelatedToJobFunction.class);
	private final JobService jobService;

	public IssuesRelatedToJobFunction(JobService jobService) {
		this.jobService = jobService;
	}

	@Override
	@Nonnull
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	@Nonnull
	public MessageSet validate(User user, @Nonnull FunctionOperand functionOperand, @Nonnull TerminalClause terminalClause) {
		MessageSet messages = new MessageSetImpl();
		final List<String> arguments = functionOperand.getArgs();
		if (arguments.size() != 1) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.number.of.arguments", 1));
		} else {
			if (jobService.get(arguments.get(0)).isEmpty()) {
				messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
			}
		}
		return messages;
	}

	@Override
	@Nonnull
	public List<QueryLiteral> getValues(@Nonnull QueryCreationContext queryCreationContext, @Nonnull final FunctionOperand functionOperand,
	                                    @Nonnull TerminalClause terminalClause) {
		Iterable<QueryLiteral> literals = Lists.newArrayList();
		List<String> arguments = functionOperand.getArgs();
		final List<Job> jobs = jobService.get(arguments.get(0));
		for (Job job : jobs) {
			literals = Iterables.concat(literals, Iterables.transform(jobService.getRelatedIssuesKeys(job),
					new Function<String,
					QueryLiteral>() {
				@Override
				public QueryLiteral apply(@Nullable String issueKey) {
					return new QueryLiteral(functionOperand, issueKey);
				}
			}));
		}
		return ImmutableList.copyOf(literals);
	}

}
