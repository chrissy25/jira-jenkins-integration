/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.webwork;

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;

import java.net.URLEncoder;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@WebSudoRequired
public class RefreshJobList extends JiraWebActionSupport {

	private final SiteService siteService;
	private final JobService jobService;
	private int siteId;

	public RefreshJobList(SiteService siteService, JobService jobService) {
		this.siteService = siteService;
		this.jobService = jobService;
	}

	@Override
	@RequiresXsrfCheck
	protected String doExecute() throws Exception {
		Site site = siteService.get(siteId);
		jobService.syncJobList(site);
		return getRedirect("ConfigureJenkinsIntegration!default.jspa?alt_token=" + URLEncoder.encode(getXsrfToken(),
				"UTF-8"));
	}

	/**
	 * Getter for the site id
	 *
	 * @return the site id
	 */
	public int getSiteId() {
		return siteId;
	}

	/**
	 * Setter for the site id
	 *
	 * @param siteId the site id to set
	 */
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

}
