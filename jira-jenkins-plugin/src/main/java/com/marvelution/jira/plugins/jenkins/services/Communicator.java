/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;

import java.net.URI;
import java.net.URL;
import java.util.List;

/**
 * Communicator Service
 * Used for communicating with the remote Jenkins site using the Applications Links
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface Communicator {

	/**
	 * Get the current online status of the remote application
	 *
	 * @return {@code true} in case the remote application is online, {@code false} in case its offline on not
	 *         responding correctly
	 */
	boolean isRemoteOnline();

	/**
	 * Get the current online status of the remote application.
	 * And also check if remote is accessible using the configured outgoing authentication provider.
	 *
	 * @return {@code true} in case the remote application is online, {@code false} in case its offline on not
	 *         responding correctly
	 * @throws CredentialsRequiredException in case the remote is not accessible
	 */
	boolean isRemoteOnlineAndAccessible() throws CredentialsRequiredException;

	/**
	 * Check whether the remote application supports back linking
	 *
	 * @return {@code true} if back linking is supported, {@code false} otherwise
	 */
	boolean isJenkinsPluginInstalled();

	/**
	 * Configure a backlink for the {@link Job} given
	 *
	 * @param job the {@link Job} to configure the backlink for
	 */
	void registerBuildNotifier(Job job);

	/**
	 * Get all the Jobs from the remote application
	 *
	 * @return {@link List} of all the {@link Job} objects
	 */
	List<Job> getJobs();

	/**
	 * Get all the details for the given {@link Job}
	 *
	 * @param job the {@link Job}
	 * @return the detailed {@link Job}
	 */
	Job getDetailedJob(Job job);

	/**
	 * Get the {@link URL} of the given {@link Job}
	 *
	 * @param job the {@link Job}
	 * @return the {@link URL}
	 */
	URI getJobUrl(Job job);

	/**
	 * Get all the details of a {@link Build}
	 *
	 * @param job   the {@link Job} to get a build from
	 * @param build the {@link Build} to get more details on
	 * @return the {@link Build} with all the details
	 */
	Build getDetailedBuild(Job job, Build build);

	/**
	 * Get all the details of a {@link Build}
	 *
	 * @param job    the {@link Job} to get a build from
	 * @param number the build number to get
	 * @return the {@link Build} with all the details
	 */
	Build getDetailedBuild(Job job, int number);

	/**
	 * Get the {@link URL} of the given {@link Job}
	 *
	 * @param job   the {@link Job}
	 * @param build the {@link Build}
	 * @return the {@link URL}
	 */
	URI getBuildUrl(Job job, Build build);

}
