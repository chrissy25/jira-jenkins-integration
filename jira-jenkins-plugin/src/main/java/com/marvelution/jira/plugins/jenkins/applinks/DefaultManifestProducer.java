/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestProducer;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.net.*;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

/**
 * Default typed {@link com.atlassian.applinks.spi.manifest.ManifestProducer}
 * This types producer will try to get the manifest using the system {@link com.atlassian.applinks.spi.manifest.ManifestRetriever} and
 * will default to a new {@link DefaultManifest} in case the manifest cannot be downloaded from the remote system.
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
public abstract class DefaultManifestProducer<T extends NonAppLinksApplicationType> implements ManifestProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsManifestProducer.class);
	private static final int CONNECTION_TIMEOUT = 10000;
	private final RequestFactory<Request<Request<?, Response>, Response>> requestFactory;
	private final ManifestRetriever manifestRetriever;
	protected final TypeAccessor typeAccessor;

	protected DefaultManifestProducer(RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
	                                  ManifestRetriever manifestRetriever, TypeAccessor typeAccessor) {
		this.requestFactory = requestFactory;
		this.manifestRetriever = manifestRetriever;
		this.typeAccessor = typeAccessor;
	}

	@Override
	public Manifest getManifest(final URI url) throws ManifestNotFoundException {
		try {
			return manifestRetriever.getManifest(url);
		} catch (ManifestNotFoundException e) {
			LOGGER.warn("Unable to get the manifest for {}", url);
			return new DefaultManifest(typeAccessor.getApplicationType(getApplicationType()), url);
		}
	}

	/**
	 * Getter for the application link type class. Used to construct the {@link com.marvelution.jira.plugins.jenkins.applinks
	 * .DefaultManifest}
	 *
	 * @return the application link type class
	 */
	protected abstract Class<T> getApplicationType();

	@Override
	public ApplicationStatus getStatus(URI url) {
		try {
			LOGGER.debug("Querying " + url + " for its online status.");
			final Request<Request<?, Response>, Response> request = requestFactory.createRequest(Request.MethodType.GET, url.toString());
			request.setConnectionTimeout(CONNECTION_TIMEOUT).setSoTimeout(CONNECTION_TIMEOUT);
			return request.executeAndReturn(new ReturningResponseHandler<Response, ApplicationStatus>() {
				@Override
				public ApplicationStatus handle(final Response response) throws ResponseException {
					return response.isSuccessful() || (response.getStatusCode() == HttpStatus.SC_FORBIDDEN) || (response.getStatusCode()
							== HttpStatus.SC_UNAUTHORIZED) ? ApplicationStatus.AVAILABLE : ApplicationStatus.UNAVAILABLE;
				}

			});
		} catch (ResponseException re) {
			return ApplicationStatus.UNAVAILABLE;
		}
	}

}
