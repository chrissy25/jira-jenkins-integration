/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.upgrade;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import java.util.Collection;
import java.util.Collections;

/**
 * {@link PluginUpgradeTask} implementation to cleanup the Jenkins and Hudson data
 *
 * @author Mark Rekveld
 * @since 1.4.7
 */
public class SiteCleanupUpgradeTask implements PluginUpgradeTask {

	private final JenkinsPluginUtil pluginUtil;
	private final SiteService siteService;
	private final ApplicationLinkService applicationLinkService;

	public SiteCleanupUpgradeTask(JenkinsPluginUtil pluginUtil, SiteService siteService, ApplicationLinkService applicationLinkService) {
		this.pluginUtil = pluginUtil;
		this.siteService = siteService;
		this.applicationLinkService = applicationLinkService;
	}

	@Override
	public int getBuildNumber() {
		return 1;
	}

	@Override
	public String getShortDescription() {
		return "Cleanup plugin site data";
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		for (Site site : siteService.getAll(false)) {
			boolean cleanup;
			try {
				cleanup = applicationLinkService.getApplicationLink(site.getApplicationId()) == null;
			} catch (TypeNotInstalledException e) {
				cleanup = true;
			}
			if (cleanup) {
				siteService.removeSite(site);
			}
		}
		return Collections.emptyList();
	}

	@Override
	public String getPluginKey() {
		return pluginUtil.getPluginKey();
	}

}
