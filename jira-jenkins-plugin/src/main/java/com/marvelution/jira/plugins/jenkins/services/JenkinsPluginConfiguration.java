/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services;

import com.atlassian.jira.project.Project;

import java.util.Set;

/**
 * @author Mark Rekveld
 * @since 1.6.0
 */
public interface JenkinsPluginConfiguration {

	int DEFAULT_MAX_BUILDS_PER_PAGE = 100;

	/**
	 * Returns the Base URL of the JIRA instance the plugin is installed on
	 */
	String getJiraBaseUrl();

	/**
	 * Returns the maximum number of builds ({@literal -1} no maximum) to display on a single page.
	 */
	int getMaximumBuildsPerPage();

	/**
	 * Sets the maximum number of builds to display on a single page.
	 *
	 * @param maximumBuildsPerPage the maximum number of builds to display, may be {@literal null} to reset to the default, or {@literal-1}
	 *                             to specify no limit.
	 */
	void setMaximumBuildsPerPage(Integer maximumBuildsPerPage);

	/**
	 * Returns the {@link Set} of project keys that the panels enabled for, may be {@literal null} to specify that all projects are
	 * enabled.
	 */
	Set<String> getEnabledProjects();

	/**
	 * Sets the {@link Set} of project keys that this plugin is enabled for.
	 *
	 * @param enabledProjects the {@link Set} of project keys to enable the panels for, {@literal null} or {@literal empty} to indicate
	 *                        all projects are enabled
	 */
	void setEnabledProjects(Set<String> enabledProjects);

	/**
	 * Returns whether the given project is explicitly enabled
	 */
	boolean isProjectEnabled(Project project);

	/**
	 * Returns whether the given project is explicitly enabled
	 */
	boolean isProjectEnabledExplicitly(Project project);

}
