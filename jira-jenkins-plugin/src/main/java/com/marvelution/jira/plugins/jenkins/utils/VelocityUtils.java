/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.utils;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

/**
 * Utility class for use in Velocity templates
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class VelocityUtils {

	private final DateTimeFormatter dateTimeFormatter;
	private final PeriodFormatter periodFormatter;

	public VelocityUtils(DateTimeFormatterFactory dateTimeFormatterFactory) {
		dateTimeFormatter = dateTimeFormatterFactory.formatter();
		periodFormatter = new PeriodFormatterBuilder()
				.printZeroRarelyLast()
				.appendDays().appendSuffix("d ")
				.appendHours().appendSuffix("h ")
				.appendMinutes().appendSuffix("m ")
				.appendSeconds().appendSuffix("s")
				.toFormatter();
	}

	/**
	 * Format the given {@link Date}
	 *
	 * @param dateInPast the {@link Date} to format
	 * @return the {@link Date} format using the {@link DateTimeStyle#RELATIVE} style
	 */
	public String getRelativePastDate(Date dateInPast) {
		return dateTimeFormatter.forLoggedInUser().withStyle(DateTimeStyle.RELATIVE).format(dateInPast);
	}

	/**
	 * Format the given duration
	 *
	 * @param duration the duration to format
	 * @return the formatter duration
	 */
	public String getDurationString(Long duration) {
		return StringUtils.trim(periodFormatter.print(new Period((long) duration)));
	}

	/**
	 * URL Encode the given string
	 *
	 * @param url the url to encode
	 * @return the encoded url
	 */
	public String urlEncode(String url) {
		try {
			return URLEncoder.encode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return url;
		}
	}

}
