/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.panels;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.action.IssueActionComparator;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.utils.VelocityUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * CI Build Panel Helper
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BuildPanelHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildPanelHelper.class);
	private final JenkinsPluginConfiguration pluginConfiguration;
	private final JobService jobService;
	private final BuildService buildService;
	private final VelocityUtils velocityUtils;
	private final PermissionManager permissionManager;
	private final TemplateRenderer templateRenderer;
	private final SearchProvider searchProvider;

	public BuildPanelHelper(JenkinsPluginConfiguration pluginConfiguration, JobService jobService, BuildService buildService,
	                        PermissionManager permissionManager, TemplateRenderer templateRenderer, SearchProvider searchProvider,
	                        VelocityUtils velocityUtils) {
		this.pluginConfiguration = pluginConfiguration;
		this.jobService = jobService;
		this.buildService = buildService;
		this.velocityUtils = velocityUtils;
		this.permissionManager = permissionManager;
		this.templateRenderer = templateRenderer;
		this.searchProvider = searchProvider;
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Issue}
	 *
	 * @param issue the {@link Issue} to get all the builds for
	 * @return the collection of {@link Build}s
	 */
	public Iterable<? extends Build> getBuildsByRelation(Issue issue) {
		LOGGER.debug("Looking for builds related to issue [{}]", issue.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInIssueKeys().add(issue.getKey());
		return buildService.getLatestBuildsByFilter(pluginConfiguration.getMaximumBuildsPerPage(), filter);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Project}
	 *
	 * @param project the {@link Project} to get all the builds for
	 * @return the collection of {@link Build}s
	 */
	public Iterable<? extends Build> getBuildsByRelation(Project project) {
		LOGGER.debug("Looking for builds related to project [{}]", project.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInProjectKeys().add(project.getKey());
		return buildService.getLatestBuildsByFilter(pluginConfiguration.getMaximumBuildsPerPage(), filter);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link ProjectComponent}
	 *
	 * @param component the {@link ProjectComponent} to get all the builds for
	 * @param user      the current logged in {@link User}
	 * @return the collection of {@link Build}s
	 */
	public Iterable<? extends Build> getBuildsByRelation(ProjectComponent component, User user) {
		LOGGER.debug("Looking for builds related to component [{}]", component.getName());
		return getBuildsByRelation(JqlQueryBuilder.newBuilder().where().project(component.getProjectId()).and().addClause(JqlQueryBuilder
				.newClauseBuilder().component(component.getId()).buildClause()).endWhere(), user);
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Version}
	 *
	 * @param version the {@link Version} to get all the builds for
	 * @param user    the current logged in {@link User}
	 * @return the collection of {@link Build}s
	 */
	public Iterable<? extends Build> getBuildsByRelation(Version version, User user) {
		LOGGER.debug("Looking for builds related to version [{}]", version.getName());
		return getBuildsByRelation(JqlQueryBuilder.newBuilder().where().project(version.getProjectObject().getKey()).and().addClause
				(JqlQueryBuilder.newClauseBuilder().affectedVersion(version.getName()).or().fixVersion(version.getId()).buildClause())
				.endWhere(), user);
	}

	/**
	 * Get all the builds related to issues that match the given {@link JqlQueryBuilder}
	 *
	 * @param jql  the {@link JqlQueryBuilder} match issues against
	 * @param user the current logged in {@link User}
	 * @return the collection of builds that are related to issues matching th JQL,
	 *         may be {@code empty} but never {@code null}
	 */
	private Iterable<? extends Build> getBuildsByRelation(JqlQueryBuilder jql, User user) {
		try {
			SearchResults results = searchProvider.search(jql.buildQuery(), user, PagerFilter.getUnlimitedFilter());
			if (results.getTotal() > 0) {
				BuildIssueFilter filter = new BuildIssueFilter();
				Iterable<String> keys = Iterables.transform(results.getIssues(), new Function<Issue, String>() {
					@Override
					public String apply(Issue issue) {
						return issue.getKey();
					}
				});
				Iterables.addAll(filter.getInIssueKeys(), keys);
				return buildService.getLatestBuildsByFilter(pluginConfiguration.getMaximumBuildsPerPage(), filter);
			}
		} catch (SearchException e) {
			LOGGER.debug("JQL " + jql.buildQuery().getQueryString() + " failed: " + e.getMessage());
		}
		return Lists.newArrayList();
	}

	/**
	 * Get {@link BuildAction}s for all the {@link Build}s given
	 *
	 * @param builds the {@link Build}s to get actions for
	 * @param sorted flag to sort the {@link BuildAction}s in reverse order, if false the list is not sorted!
	 * @return the {@link BuildAction}s
	 */
	public List<BuildAction> getBuildActions(Iterable<? extends Build> builds, boolean sorted) {
		List<BuildAction> buildActions = Lists.newArrayList();
		for (Build build : builds) {
			try {
				String html = getBuildActionHtml(build);
				if (StringUtils.isNotBlank(html)) {
					buildActions.add(new BuildAction(html, build.getBuildDate()));
				}
			} catch (Exception e) {
				LOGGER.debug("Failed to render HTML for Build [{}], {}", build, e.getMessage());
			}
		}
		if (buildActions.isEmpty()) {
			buildActions.add(BuildAction.NO_BUILDS_ACTION);
		}
		if (sorted) {
			Collections.sort(buildActions, IssueActionComparator.COMPARATOR);
			Collections.reverse(buildActions);
		}
		return buildActions;
	}

	/**
	 * Generate a HTML View for the {@link Build} given
	 *
	 * @param build the {@link Build} to generate the HTML view for
	 * @return the HTML string, may be {@code empty} but not {@code null}
	 * @throws Exception in case of errors rendering the HTML
	 */
	public String getBuildActionHtml(Build build) throws Exception {
		Job job = jobService.get(build.getJobId());
		if (job == null) {
			return "";
		}
		Map<String, Object> context = Maps.newHashMap();
		context.put("baseUrl", pluginConfiguration.getJiraBaseUrl());
		context.put("velocityUtils", velocityUtils);
		context.put("build", build);
		context.put("buildUrl", jobService.getJobBuildUrl(job, build));
		context.put("job", job);
		context.put("jobUrl", jobService.getJobUrl(job));
		if (buildService.getRelatedIssueKeyCount(build) <= 5) {
			context.put("issueKeys", Lists.newArrayList(buildService.getRelatedIssueKeys(build)));
		} else {
			context.put("projectKeys", Lists.newArrayList(buildService.getRelatedProjectKeys(build)));
		}
		StringWriter writer = new StringWriter();
		try {
			templateRenderer.render("/templates/jenkins-build.vm", context, writer);
		} catch (IOException e) {
			LOGGER.warn("Failed to render html for build {} -> {}", build.getId(), e.getMessage());
		}
		return writer.toString();
	}

	/**
	 * Returns whether the panel should be shown for the given project and user
	 */
	public boolean showPanel(Project project, User user) {
		return permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, project, user) &&
				pluginConfiguration.isProjectEnabled(project);
	}

	/**
	 * Returns whether the panel should be shown for the given issue and user
	 */
	public boolean showPanel(Issue issue, User user) {
		return permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, issue, user) &&
				pluginConfiguration.isProjectEnabled(issue.getProjectObject());
	}

}
