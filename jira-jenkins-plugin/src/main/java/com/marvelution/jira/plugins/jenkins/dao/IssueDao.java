/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao;

import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;

/**
 * Issue Data Access interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface IssueDao {

	/**
	 * Get all the related Issue Keys for the {@link Build} given
	 *
	 * @param build the {@link Build} to get the issue keys for
	 * @return the collection of all the related issue keys
	 */
	Iterable<String> getIssueKeysByBuild(Build build);

	/**
	 * Get all the related Issue Keys for the {@link Job} given
	 *
	 * @param job the {@link Job} to get the issue keys for
	 * @return the collection of all the related issue keys
	 */
	Iterable<String> getIssueKeysByJob(Job job);

	/**
	 * Get all the related Project Keys for the {@link Build} given
	 *
	 * @param build the {@link Build} to get the project keys for
	 * @return the collection of all the related project keys
	 */
	Iterable<String> getProjectKeysByBuild(Build build);

	/**
	 * Get the count of {@link com.marvelution.jira.plugins.jenkins.ao.IssueMapping}s for the given {@link Build}
	 *
	 * @param build the {@link Build}
	 * @return the count, zero or higher
	 */
	int getIssueLinkCount(Build build);

	/**
	 * Create a new link between the given {@link Build} and issue key
	 *
	 * @param build the {@link Build}
	 * @param issueKey the issue key to link with
	 * @return {@code true} in case of a successful link, {@code false} otherwise
	 */
	boolean link(Build build, String issueKey);

	/**
	 * Unlink all builds related to the {@code issueKey} given
	 *
	 * @param issueKey the {@code issueKey} unlink
	 */
	void unlinkForIssueKey(String issueKey);

	/**
	 * Unlink all builds related to the {@code projectKey} given
	 *
	 * @param projectKey the {@code projectKey} unlink
	 */
	void unlinkForProjectKey(String projectKey);

}
