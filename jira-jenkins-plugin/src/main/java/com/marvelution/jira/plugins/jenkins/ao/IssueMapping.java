/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Issue Mappings
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Preload
public interface IssueMapping extends Entity {

	String PORJECT_KEY = "PROJECT_KEY";
	String ISSUE_KEY = "ISSUE_KEY";
	String BUILD_ID = "BUILD_ID";
	String JOB_ID = "JOB_ID";
	String BUILD_DATE = "BUILD_DATE";

	String getProjectKey();

	void setProjectKey(String projectKey);

	String getIssueKey();

	void setIssueKey(String issueKey);

	int getBuildId();

	void setBuildId(int buildId);

	int getJobId();

	void setJobId(int jobId);

	long getBuildDate();

	void setBuildDate(long buildDate);

}
