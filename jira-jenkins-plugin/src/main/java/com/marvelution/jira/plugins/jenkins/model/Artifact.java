/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Artifact model class
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement(name = "artifact")
@XmlAccessorType(XmlAccessType.FIELD)
public class Artifact {

	private String fileName;
	private String displayPath;
	private String relativePath;

	/**
	 * Default constructor for JAXB
	 */
	Artifact() {
	}

	public Artifact(String fileName, String displayPath, String relativePath) {
		this.fileName = fileName;
		this.displayPath = displayPath;
		this.relativePath = relativePath;
	}

	/**
	 * Getter for the filename
	 *
	 * @return the filename
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Getter for the display path
	 *
	 * @return the display path
	 */
	public String getDisplayPath() {
		return displayPath;
	}

	/**
	 * Getter for the relative path
	 *
	 * @return the relative path
	 */
	public String getRelativePath() {
		return relativePath;
	}

}
