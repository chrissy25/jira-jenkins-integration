/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonApplicationType;
import com.marvelution.jira.plugins.jenkins.dao.SiteDao;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.model.SiteType;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Default {@link SiteService} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultSiteService implements SiteService {

	private final Logger LOGGER = LoggerFactory.getLogger(SiteService.class);
	private final ApplicationLinkService applicationLinkService;
	private final SiteDao siteDao;
	private final JobService jobService;

	public DefaultSiteService(ApplicationLinkService applicationLinkService, SiteDao siteDao, JobService jobService) {
		this.applicationLinkService = applicationLinkService;
		this.siteDao = siteDao;
		this.jobService = jobService;
	}

	@Override
	public Site get(int siteId) {
		Site site = siteDao.get(siteId);
		if (validateAndEnrichSite(site, null)) {
			return site;
		} else {
			return null;
		}
	}

	@Override
	public List<Site> getAll(final boolean includeJobs) {
		return Lists.newArrayList(Iterables.filter(Lists.transform(siteDao.getAll(), new Function<Site, Site>() {
			@Override
			public Site apply(@Nullable Site from) {
				if (from != null && validateAndEnrichSite(from, null)) {
					if (includeJobs) {
						from.setJobs(jobService.getAllBySite(from, true));
					}
					return from;
				} else {
					return null;
				}
			}
		}), Predicates.notNull()));
	}

	/**
	 * Validate and enrich the given {@link Site} with information from the related {@link ApplicationLink}
	 * <p/>
	 * If the site is not valid, then it will also be removed from the data store.
	 *
	 * @param site            the {@link Site} to enrich
	 * @param applicationLink the {@link ApplicationLink} to get the data from
	 * @return {@code true} if the site is valid and enriched with the {@link com.atlassian.applinks.api.ApplicationLink} information
	 */
	private boolean validateAndEnrichSite(Site site, ApplicationLink applicationLink) {
		if (site != null) {
			if (applicationLink == null) {
				applicationLink = getApplicationLink(site);
			}
			if (applicationLink == null) {
				LOGGER.info("ApplicationLink for Site {} is no longer available... removing site to prevent errors.", site.getName());
				removeSite(site);
				return false;
			}
			site.setDisplayUrl(applicationLink.getDisplayUrl().toString());
			if (applicationLink.getType() instanceof HudsonApplicationType) {
				site.setType(SiteType.HUDSON);
			} else {
				site.setType(SiteType.JENKINS);
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Site getByApplicationLink(ApplicationLink applicationLink) {
		Site site = getByApplicationLinkInternal(applicationLink);
		if (site == null) {
			site = new Site();
			site.setApplicationId(applicationLink.getId());
			site.setName(applicationLink.getName());
			site = siteDao.save(site);
		}
		validateAndEnrichSite(site, applicationLink);
		return site;
	}

	private Site getByApplicationLinkInternal(ApplicationLink applicationLink) {
		return siteDao.get(applicationLink.getId());
	}

	@Override
	public Site save(Site site) {
		return siteDao.save(site);
	}

	@Override
	public void enable(int siteId, boolean enabled) {
		Site site = get(siteId);
		if (site != null) {
			site.setAutoLink(enabled);
			siteDao.save(site);
		}
	}

	@Override
	public void removeByApplicationLink(ApplicationLink applicationLink) {
		Site site = getByApplicationLinkInternal(applicationLink);
		if (site != null) {
			removeSite(site);
		}
	}

	@Override
	public void removeSite(Site site) {
		siteDao.remove(site.getId());
		jobService.removeAllFromSite(site);
	}

	@Override
	public void updateApplicationLink(ApplicationLink newApplicationLink, ApplicationId oldApplicationId) {
		Site site = siteDao.get(oldApplicationId);
		site.setApplicationId(newApplicationLink.getId());
		site.setName(newApplicationLink.getName());
		siteDao.save(site);
	}

	@Override
	public ApplicationLink getApplicationLink(Site site) {
		if (site.getApplicationId() == null && site.getId() > 0) {
			site = get(site.getId());
		}
		try {
			return applicationLinkService.getApplicationLink(site.getApplicationId());
		} catch (TypeNotInstalledException e) {
			throw new IllegalArgumentException("Unable to get an ApplicationLink for " + site.getApplicationId());
		}
	}

	@Override
	public ApplicationLink getApplicationLink(Job job) {
		if (job.getSiteId() > 0) {
			return getApplicationLink(get(job.getSiteId()));
		} else {
			throw new IllegalArgumentException("Job '" + job.getName() + "' is not linked to a known Jenkins Site");
		}
	}

}
