/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.marvelution.jira.plugins.jenkins.model.*;
import com.marvelution.jira.plugins.jenkins.rest.security.AdminRequired;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.JobService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST Resource for {@link Job}s
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Path("job")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class JobResource {

	private final JobService jobService;
	private final BuildService buildService;

	public JobResource(JobService jobService, BuildService buildService) {
		this.jobService = jobService;
		this.buildService = buildService;
	}

	/**
	 * Get all the Jobs available
	 *
	 * @param includeDeleted flag where to include or exclude deleted jobs
	 * @return collections of Jobs
	 */
	@GET
	@AdminRequired
	public Response getAll(@QueryParam("includeDeleted") @DefaultValue("false") boolean includeDeleted) {
		List<Job> jobs = jobService.getAll(includeDeleted);
		return Response.ok(new JobList(jobs)).build();
	}

	/**
	 * Get a single Job by its Id
	 *
	 * @param jobId the job Id
	 * @return 200 OK (With the Job) if there is a job with the given id, 404 NOT FOUND otherwise
	 */
	@GET
	@AdminRequired
	@Path("{jobId}")
	public Response getJob(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		} else {
			return Response.ok(job).build();
		}
	}

	/**
	 * Get the current synchronization status of a job
	 *
	 * @param jobId the job id
	 * @return 200 OK with {@link Progress} status, 204 NO CONTENT or 404 NOT FOUND if the job is not found
	 */
	@GET
	@AdminRequired
	@Path("{jobId}/sync/status")
	public Response getSyncStatus(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		} else {
			Progress progress = jobService.getSyncStatus(job);
			if (progress != null) {
				return Response.ok(progress).build();
			} else {
				return Response.noContent().build();
			}
		}
	}

	/**
	 * Trigger the synchronization of a specific Job
	 *
	 * @param jobId the Id of the Job to synchronize
	 * @return 204 NO CONTENT if there is a job with the given id, 404 NOT FOUND otherwise
	 */
	@POST
	@AnonymousAllowed
	@Path("{jobId}/sync")
	public Response syncJob(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job != null) {
			jobService.sync(jobId);
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		}
	}

	/**
	 * Enable or Disable the auto linking of the job
	 *
	 * @param jobId    the id of the job
	 * @param restData the {@link RestData} payload send
	 * @return 204 NO CONTENT if the job is found, 404 NOT FOUND otherwise
	 */
	@POST
	@AdminRequired
	@Path("{jobId}/autolink")
	public Response enableJobAutoLink(@PathParam("jobId") int jobId, RestData restData) {
		Job job = jobService.get(jobId);
		if (job != null) {
			jobService.enable(jobId, Boolean.parseBoolean(restData.getPayload()));
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		}
	}

	/**
	 * Get the last build date
	 *
	 * @param jobId the jobIId to get the last build time for
	 * @return 200 OK with the last build time as a long, 204 NO CONTENT if the job has not been build yet,
	 *         404 NOT FOUND if the job cannot be found
	 */
	@GET
	@AnonymousAllowed
	@Path("{jobId}/lastbuildtime")
	public Response getLastBuildTime(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job != null) {
			if (job.getLastBuild() > 0) {
				Build build = buildService.get(job, job.getLastBuild());
				if (build != null) {
					return Response.ok(new RestData(String.valueOf(build.getTimestamp()))).build();
				}
			}
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		}
	}

	/**
	 * Remove all the builds of a job
	 *
	 * @param jobId the id of the job to remove all the builds from
	 * @return 204 NO CONTENT when the job if found the remove service is called,
	 *         404 NOT FOUND in case the job cannot be found
	 */
	@DELETE
	@AdminRequired
	@Path("{jobId}/builds")
	public Response removeAllBuilds(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job != null) {
			buildService.removeAllInJob(job);
			job.setLastBuild(0);
			jobService.save(job);
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		}
	}

	/**
	 * Mark all the builds of a job as deleted and start a full sync to rebuild the build cache
	 *
	 * @param jobId the job Id to restart the cache on
	 * @return 204 NO CONTENT when the job is found and the rebuild is started
	 *         404 NOT FOUND in case the job is not found
	 */
	@POST
	@AdminRequired
	@Path("{jobId}/rebuild")
	public Response rebuildJobCache(@PathParam("jobId") int jobId) {
		Job job = jobService.get(jobId);
		if (job != null) {
			buildService.deleteAllInJob(job);
			job.setLastBuild(0);
			jobService.save(job);
			jobService.sync(job.getId());
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No Job with id: " + jobId).build();
		}
	}

}
