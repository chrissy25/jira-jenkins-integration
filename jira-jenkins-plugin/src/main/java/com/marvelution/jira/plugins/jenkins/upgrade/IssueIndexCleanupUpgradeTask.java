/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.upgrade;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import com.marvelution.jira.plugins.jenkins.dao.IssueDao;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.marvelution.jira.plugins.jenkins.ao.IssueMapping.ISSUE_KEY;

/**
 * {@link PluginUpgradeTask} implementation to cleanup deleted issues form the build index
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IssueIndexCleanupUpgradeTask implements PluginUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssueIndexCleanupUpgradeTask.class);
	private final JenkinsPluginUtil pluginUtil;
	private final ActiveObjects ao;
	private final IssueDao issueDao;
	private final IssueManager issueManager;

	public IssueIndexCleanupUpgradeTask(JenkinsPluginUtil pluginUtil, ActiveObjects ao, IssueDao issueDao, IssueManager issueManager) {
		this.pluginUtil = pluginUtil;
		this.ao = ao;
		this.issueDao = issueDao;
		this.issueManager = issueManager;
	}

	@Override
	public int getBuildNumber() {
		return 3;
	}

	@Override
	public String getShortDescription() {
		return "Cleanup deleted issues from the build index";
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		Set<String> issueKeys = ao.executeInTransaction(new TransactionCallback<Set<String>>() {
			@Override
			public Set<String> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(ISSUE_KEY + " IS NOT NULL"));
				return Sets.newHashSet(Lists.transform(Arrays.asList(mappings), new Function<IssueMapping, String>() {
					@Override
					public String apply(IssueMapping input) {
						return input != null ? input.getIssueKey() : null;
					}
				}));
			}
		});
		for (String issueKey : issueKeys) {
			try {
				if (issueKey != null && issueManager.getIssueObject(issueKey) == null) {
					LOGGER.info("Found deleted issue {}. Removing it form the build index", issueKey);
					issueDao.unlinkForIssueKey(issueKey);
				}
			} catch (DataAccessException e) {
				LOGGER.error("Failed to retrieve issue by key [{}]; {}", issueKey, e.getMessage());
			}
		}
		return Collections.emptyList();
	}

	@Override
	public String getPluginKey() {
		return pluginUtil.getPluginKey();
	}

}
