/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.listeners;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.event.ApplicationLinkEvent;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonApplicationType;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;

/**
 * Base ApplicationLink Event listener
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractJenkinsApplicationLinkListener extends AbstractEventListener {

	protected AbstractJenkinsApplicationLinkListener(EventPublisher eventPublisher) {
		super(eventPublisher);
	}

	/**
	 * Check if the given {@link ApplicationLinkEvent} event object can be handled by this listener implementation
	 *
	 * @param event the {@link ApplicationLinkEvent} event to check
	 * @return {@code true} if the {@link ApplicationType} of the event is a {@link JenkinsApplicationType} or {@link HudsonApplicationType}
	 */
	protected boolean supportsApplicationLinkType(ApplicationLinkEvent event) {
		ApplicationType applicationType = event.getApplicationType();
		return applicationType instanceof JenkinsApplicationType || applicationType instanceof HudsonApplicationType;
	}

}
