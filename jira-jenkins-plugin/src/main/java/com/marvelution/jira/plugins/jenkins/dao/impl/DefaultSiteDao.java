/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.ao.SiteMapping;
import com.marvelution.jira.plugins.jenkins.dao.SiteDao;
import com.marvelution.jira.plugins.jenkins.model.Site;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Default {@link SiteDao} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultSiteDao implements SiteDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSiteDao.class);
	private final ActiveObjects ao;
	private final Function<SiteMapping, Site> siteMappingToSiteFunction = new Function<SiteMapping, Site>() {
		@Override
		public Site apply(@Nullable SiteMapping from) {
			if (from == null) {
				return null;
			}
			Site site = new Site(from.getID());
			site.setApplicationId(new ApplicationId(from.getApplinkId()));
			site.setName(from.getName());
			site.setSupportsBackLink(from.isSupportsBackLink());
			site.setAutoLink(from.isAutoLink());
			return site;
		}
	};

	public DefaultSiteDao(ActiveObjects ao) {
		this.ao = ao;
	}

	@Override
	public Site get(final int siteId) {
		SiteMapping mapping = ao.executeInTransaction(new TransactionCallback<SiteMapping>() {
			@Override
			public SiteMapping doInTransaction() {
				return ao.get(SiteMapping.class, siteId);
			}
		});
		return siteMappingToSiteFunction.apply(mapping);
	}

	@Override
	public Site get(final ApplicationId applicationId) {
		SiteMapping mapping = ao.executeInTransaction(new TransactionCallback<SiteMapping>() {
			@Override
			public SiteMapping doInTransaction() {
				SiteMapping[] mappings = ao.find(SiteMapping.class, Query.select().where(SiteMapping.APPLINK_ID + " = ?",
						applicationId.get()));
				if (mappings != null && mappings.length == 1) {
					return mappings[0];
				} else {
					return null;
				}
			}
		});
		return siteMappingToSiteFunction.apply(mapping);
	}

	@Override
	public List<Site> getAll() {
		final List<SiteMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<SiteMapping>>() {
			@Override
			public List<SiteMapping> doInTransaction() {
				return Arrays.asList(ao.find(SiteMapping.class, Query.select()));
			}
		});
		return Lists.transform(mappings, siteMappingToSiteFunction);
	}

	@Override
	public Site save(final Site site) {
		LOGGER.debug("Saving {}", site.toString());
		SiteMapping mapping = ao.executeInTransaction(new TransactionCallback<SiteMapping>() {
			@Override
			public SiteMapping doInTransaction() {
				SiteMapping mapping;
				if (site.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(SiteMapping.APPLINK_ID, site.getApplicationId().get());
					map.put(SiteMapping.NAME, site.getName());
					map.put(SiteMapping.SUPPORTS_BACK_LINK, site.isSupportsBackLink());
					map.put(SiteMapping.AUTO_LINK, site.isAutoLink());
					mapping = ao.create(SiteMapping.class, map);
					mapping = ao.find(SiteMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(SiteMapping.class, site.getId());
					mapping.setApplinkId(site.getApplicationId().get());
					mapping.setName(site.getName());
					mapping.setSupportsBackLink(site.isSupportsBackLink());
					mapping.setAutoLink(site.isAutoLink());
					mapping.save();
				}
				return mapping;
			}
		});
		return siteMappingToSiteFunction.apply(mapping);
	}

	@Override
	public void remove(int siteId) {
		ao.delete(ao.get(SiteMapping.class, siteId));
	}

}
