/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao;

import com.marvelution.jira.plugins.jenkins.model.TestResults;

/**
 * Interface for {@link com.marvelution.jira.plugins.jenkins.model.TestResults} related Data Access
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
public interface TestResultDao {

	/**
	 * Get the {@link com.marvelution.jira.plugins.jenkins.model.TestResults} for a specific build
	 *
	 * @param buildId the build Id
	 * @return the {@link com.marvelution.jira.plugins.jenkins.model.TestResults}
	 */
	TestResults getForBuild(int buildId);

	/**
	 * Save the {@link com.marvelution.jira.plugins.jenkins.model.TestResults}
	 *
	 * @param buildId the Id of the build
	 * @param results the {@link com.marvelution.jira.plugins.jenkins.model.TestResults} to save
	 * @return the {@link com.marvelution.jira.plugins.jenkins.model.TestResults}
	 */
	TestResults save(int buildId, TestResults results);

	/**
	 * Remove all the {@link com.marvelution.jira.plugins.jenkins.model.TestResults}s linked to the given builds by buildId
	 *
	 * @param buildIds the Ids of the builds to remove the {@link com.marvelution.jira.plugins.jenkins.model.TestResults} from
	 */
	void removeTestResults(int[] buildIds);

}
