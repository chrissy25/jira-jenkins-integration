/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.model;

import com.atlassian.applinks.api.ApplicationId;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.adapters.ApplicationIdAdapter;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement(name = "site")
@XmlAccessorType(XmlAccessType.FIELD)
public class Site {

	@XmlAttribute
	private int id;
	@XmlJavaTypeAdapter(value = ApplicationIdAdapter.class, type = ApplicationId.class)
	private ApplicationId applicationId;
	private boolean supportsBackLink = false;
	private boolean autoLink = true;
	private boolean deleted = false;
	private String name;
	private String displayUrl;
	@XmlElement(name = "job")
	private List<Job> jobs;
	private SiteType type;

	/**
	 * JAXB Default Constructor
	 */
	public Site() {
		id = 0;
	}

	public Site(int id) {
		this.id = id;
	}

	/**
	 * Getter for the site Id
	 *
	 * @return the site id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for the site {@link ApplicationId}
	 *
	 * @return the {@link ApplicationId}
	 */
	public ApplicationId getApplicationId() {
		return applicationId;
	}

	/**
	 * Setter for the site {@link ApplicationId}
	 *
	 * @param applicationId the site {@link ApplicationId}
	 */
	public void setApplicationId(ApplicationId applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * Getter for the autoLink support state
	 *
	 * @return the autoLink support state
	 */
	public boolean isSupportsBackLink() {
		return supportsBackLink;
	}

	/**
	 * Setter for the autoLink support state
	 *
	 * @param supportsBackLink the autoLink support state
	 */
	public void setSupportsBackLink(boolean supportsBackLink) {
		this.supportsBackLink = supportsBackLink;
	}

	/**
	 * Getter for the autoLink state
	 *
	 * @return the autoLink state
	 */
	public boolean isAutoLink() {
		return autoLink;
	}

	/**
	 * Setter for the autoLink state
	 *
	 * @param autoLink the new autoLink state
	 */
	public void setAutoLink(boolean autoLink) {
		this.autoLink = autoLink;
	}

	/**
	 * Getter for the delete state
	 *
	 * @return the deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the deleted state
	 *
	 * @param deleted the new deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter for the Site name
	 *
	 * @return the name of the site
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for the Site name
	 *
	 * @param name the name of the site
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the Display URL
	 *
	 * @return the display URL
	 */
	public String getDisplayUrl() {
		return displayUrl;
	}

	/**
	 * Setter for the display URL
	 *
	 * @param displayUrl the display URL
	 */
	public void setDisplayUrl(String displayUrl) {
		this.displayUrl = displayUrl;
	}

	/**
	 * Getter for the {@link Job} {@link List}
	 *
	 * @return the {@link List} of {@link Job} objects, never {@code null}
	 *         called
	 */
	public List<Job> getJobs() {
		if (jobs == null) {
			jobs = Lists.newArrayList();
		}
		return jobs;
	}

	/**
	 * Setter for the {@link Job} {@link List}
	 *
	 * @param jobs the {@link Job} {@link List}
	 */
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	/**
	 * Getter for the {@link SiteType}
	 *
	 * @return the {@link SiteType}
	 */
	public SiteType getType() {
		return type;
	}

	/**
	 * Setter for the {@link SiteType}
	 *
	 * @param type the {@link SiteType}
	 */
	public void setType(SiteType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(applicationId).append(name).build();
	}


	@Override
	public boolean equals(Object o) {
		if (o instanceof Site) {
			Site other = (Site) o;
			return applicationId.equals(other.applicationId);
		}
		return false;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("applicationId", applicationId).add("name", name).add("autoLink",
				autoLink).add("supportsBackLink", supportsBackLink).toString();
	}

}
