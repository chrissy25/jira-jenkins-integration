/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.services.impl;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import com.marvelution.jira.plugins.jenkins.model.Artifact;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.ChangeSet;
import com.marvelution.jira.plugins.jenkins.model.Culprit;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.model.TestResults;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.utils.URIUtils;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JSON Rest Implementation of the {@link com.marvelution.jira.plugins.jenkins.services.Communicator}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class RestCommunicator implements Communicator {

	public static final int CALL_TIMEOUT = 50000;
	private static final Logger LOGGER = LoggerFactory.getLogger(RestCommunicator.class);
	private static final String[] COMMIT_ID_FIELDS = new String[] {
			"commitId", // Default
			"id", // GIT
			"node", // Mercurial
			"revision", // Subversion
			"changeItem" // Perforce
	};
	private final HostApplication hostApplication;
	private final Site site;
	private final ApplicationLink applicationLink;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;

	public RestCommunicator(HostApplication hostApplication, Site site, ApplicationLink applicationLink,
	                        AuthenticationConfigurationManager authenticationConfigurationManager) {
		this.hostApplication = hostApplication;
		this.site = site;
		this.applicationLink = applicationLink;
		this.authenticationConfigurationManager = authenticationConfigurationManager;
	}

	@Override
	public boolean isRemoteOnline() {
		try {
			return isRemoteOnlineAndAccessible();
		} catch (CredentialsRequiredException e) {
			// We only care if the remote application is online or not, not about any auth
			return true;
		}
	}

	@Override
	public boolean isRemoteOnlineAndAccessible() throws CredentialsRequiredException {
		return executeGetRequest("/api/json", CALL_TIMEOUT).isSuccessful();
	}

	@Override
	public boolean isJenkinsPluginInstalled() {
		try {
			ApplicationLinkRequestFactory requestFactory = getApplicationLinkRequestFactory();
			ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, "/jenkins-jira-plugin/static/help.html");
			RestResponse response = executeRequest(request, requestFactory, CALL_TIMEOUT);
			LOGGER.debug("Backlink support check on {}; code: {}", applicationLink.getName(), response.getStatusCode());
			return response.getStatusCode() / 100 == 2;
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
			                                                                     applicationLink.getDisplayUrl(), e.getMessage() });
		}
		return false;
	}

	@Override
	public void registerBuildNotifier(Job job) {
		if (site.isSupportsBackLink()) {
			try {
				String url = getJobUrlPart(job) + "/jira/";
				ApplicationLinkRequestFactory requestFactory = getApplicationLinkRequestFactory();
				ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.POST, url);
				String jiraBaseUrl = StringUtils.stripEnd(hostApplication.getBaseUrl().toString(), "/");
				request.addRequestParameters(
						"appId", hostApplication.getId().get(),
						"jobId", String.valueOf(job.getId()),
						"postUrl", jiraBaseUrl + "/rest/jenkins/1.0/job/" + job.getId() + "/sync"
				);
				RestResponse response = executeRequest(request, requestFactory, CALL_TIMEOUT);
				if (response.getStatusCode() / 100 != 2) {
					LOGGER.warn("Unable to register build notifier on {}:{}, {} [{}]",
					            new Object[] { applicationLink.getName(), job.getName(), response.getStatusMessage(),
					                           response.getStatusCode() });
				}
			} catch (CredentialsRequiredException e) {
				LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
				                                                                     applicationLink.getDisplayUrl(), e.getMessage() });
			}
		}
	}

	@Override
	public List<Job> getJobs() {
		List<Job> jobs = Lists.newArrayList();
		try {
			RestResponse response = executeGetRequest("/api/json", CALL_TIMEOUT);
			if (response.getJson() != null && response.getJson().has("jobs")) {
				JSONArray json = response.getJson().getJSONArray("jobs");
				LOGGER.info("Found {} jobs", new Object[] { json.length() });
				for (int index = 0; index < json.length(); index++) {
					final JSONObject jsonJob = json.getJSONObject(index);
					jobs.add(new Job(site.getId(), jsonJob.getString("name")));
				}
			} else {
				handleNoJsonResponse(response);
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
			                                                                     applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Job list from {}: {}", applicationLink.getName(), e.getMessage());
		}
		return jobs;
	}

	@Override
	public Job getDetailedJob(Job job) {
		try {
			RestResponse response = executeGetRequest(getJobUrlPart(job) + "/api/json", CALL_TIMEOUT);
			JSONObject json = response.getJson();
			if (json != null) {
				job.setDisplayName(optJsonString(json, "displayName"));
				job.setDescription(optJsonString(json, "description"));
				job.setBuildable(json.optBoolean("buildable", true));
				job.setInQueue(json.optBoolean("inQueue"));
				final JSONObject firstBuild = json.optJSONObject("firstBuild");
				if (firstBuild != null && firstBuild.has("number")) {
					job.setOldestBuild(firstBuild.optInt("number", -1));
				}
				final JSONArray builds = json.optJSONArray("builds");
				if (builds != null && builds.length() > 0) {
					for (int index = 0; index < builds.length(); index++) {
						final JSONObject jsonBuild = builds.getJSONObject(index);
						final Build build = new Build(job.getId(), jsonBuild.getInt("number"));
						job.getBuilds().add(build);
					}
				}
			} else {
				handleNoJsonResponse(response);
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
			                                                                     applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Job {} from {}: {}", new Object[] { job.getName(),
			                                                                             applicationLink.getName(), e.getMessage() });
		}
		return job;
	}

	@Override
	public URI getJobUrl(Job job) {
		return URI.create(applicationLink.getDisplayUrl() + getJobUrlPart(job));
	}

	@Override
	public Build getDetailedBuild(Job job, Build build) {
		return getDetailedBuild(job, build.getNumber());
	}

	@Override
	public Build getDetailedBuild(Job job, int number) {
		Build build = new Build(job.getId(), number);
		try {
			RestResponse response = executeGetRequest(getJobUrlPart(job) + "/" + number + "/api/json?depth=0",
			                                          CALL_TIMEOUT);
			if (response.getJson() != null) {
				final JSONObject jsonBuild = response.getJson();
				String displayName = optJsonString(jsonBuild, "fullDisplayName");
				if (displayName != null && !(displayName.startsWith(job.getDisplayName()) && displayName.endsWith("#" + number))) {
					// Only set the full display name if it doesn't start with the job display name and with #[number]
					// which is the default format
					build.setDisplayName(displayName);
				}
				build.setBuiltOn(optJsonString(jsonBuild, "builtOn"));
				build.setResult(optJsonString(jsonBuild, "result"));
				build.setTimestamp(jsonBuild.getLong("timestamp"));
				build.setDuration(jsonBuild.getLong("duration"));
				final JSONArray actions = jsonBuild.optJSONArray("actions");
				if (actions != null) {
					for (int index = 0; index < actions.length(); index++) {
						final JSONObject object = actions.optJSONObject(index);
						if (object == null) {
							continue;
						}
						// Look for the build cause
						if (object.has("causes")) {
							JSONArray causes = object.optJSONArray("causes");
							for (int ii = 0; ii < causes.length(); ii++) {
								final JSONObject cause = causes.getJSONObject(ii);
								if (cause.has("shortDescription")) {
									// There should only be one build cause
									build.setCause(cause.getString("shortDescription"));
									break;
								}
							}
							// Look for the test results
						} else if (object.has("urlName") && "testReport".equals(object.getString("urlName"))) {
							// Located test results action
							build.setTestResults(new TestResults(object.getInt("failCount"), object.getInt("skipCount"),
							                                     object.getInt("totalCount")));
						}
					}
				}
				final JSONArray artifacts = jsonBuild.optJSONArray("artifacts");
				if (artifacts != null && artifacts.length() > 0) {
					for (int index = 0; index < artifacts.length(); index++) {
						final JSONObject artifact = artifacts.getJSONObject(index);
						build.getArtifacts().add(new Artifact(artifact.getString("fileName"),
						                                      artifact.getString("displayPath"), artifact.getString("relativePath")));
					}
				}
				final JSONArray culprits = jsonBuild.optJSONArray("culprits");
				if (culprits != null && culprits.length() > 0) {
					for (int index = 0; index < culprits.length(); index++) {
						final JSONObject culprit = culprits.getJSONObject(index);
						String id = optJsonString(culprit, "id");
						if (id == null && culprit.has("absoluteUrl")) {
							String absoluteUrl = culprit.getString("absoluteUrl");
							id = absoluteUrl.substring(absoluteUrl.lastIndexOf("/"));
						} else if (id == null) {
							id = culprit.getString("fullName");
						}
						build.getCulprits().add(new Culprit(id, culprit.getString("fullName")));
					}
				}
				if (jsonBuild.has("changeSet")) {
					final JSONObject changeSet = jsonBuild.optJSONObject("changeSet");
					if (changeSet != null && changeSet.has("items")) {
						final JSONArray items = changeSet.optJSONArray("items");
						if (items != null && items.length() > 0) {
							for (int index = 0; index < items.length(); index++) {
								final JSONObject item = items.getJSONObject(index);
								String commitId = String.valueOf(index);
								for (String idField : COMMIT_ID_FIELDS) {
									if (item.has(idField) && !item.isNull(idField)) {
										commitId = item.getString(idField);
										break;
									}
								}
								build.getChangeSet().add(new ChangeSet(commitId, optJsonString(item, "comment")));
							}
						}
					}
				}
			} else {
				handleNoJsonResponse(response);
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
			                                                                     applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Build {} of {} from {}: {}", new Object[] { number,
			                                                                                     job.getName(), applicationLink.getName(),
			                                                                                     e.getMessage() });
			LOGGER.debug("JSON Exception", e);
		}
		return build;
	}

	@Override
	public URI getBuildUrl(Job job, Build build) {
		URI jobUrl = getJobUrl(job);
		return URI.create(jobUrl.toString() + "/" + build.getNumber());
	}

	private void handleNoJsonResponse(RestResponse response) {
		if (response.getErrors().isEmpty()) {
			LOGGER.warn("No JSON response came from the Jenkins server");
		} else {
			LOGGER.error("The Jenkins server response leaded to errors: \n - {} \n\n Response: \n{}",
			             Joiner.on("\n - ").join(response.getErrors()), response.getResponseBody());
		}
	}

	/**
	 * Get the Url of the {@link Job} given without the Site base url
	 *
	 * @param job the {@link Job}
	 * @return the Url
	 */
	private String getJobUrlPart(Job job) {
		StringBuilder url = new StringBuilder("/job/");
		try {
			url.append(URIUtils.encode(job.getName(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			url.append(job.getName());
		}
		return url.toString();
	}

	/**
	 * Getter for the {@link ApplicationLinkRequestFactory}
	 *
	 * @return the {@link ApplicationLinkRequestFactory}
	 */
	private ApplicationLinkRequestFactory getApplicationLinkRequestFactory() {
		ApplicationLinkRequestFactory factory = applicationLink.createNonImpersonatingAuthenticatedRequestFactory();
		if (factory == null) {
			factory = applicationLink.createAuthenticatedRequestFactory(Anonymous.class);
			// IMPORTANT: Both the Impersonating and NonImpersonating Auth Provider require a Remote User Context, breaking the sync job
			if (authenticationConfigurationManager.isConfigured(applicationLink.getId(), BasicAuthenticationProvider.class)) {
				factory = new BasicAuthApplicationLinkRequestFactory(factory, authenticationConfigurationManager.getConfiguration(
						applicationLink.getId(), BasicAuthenticationProvider.class));
			}
		}
		return factory;
	}

	/**
	 * Execute a Get Request on the {@link ApplicationLink} given
	 *
	 * @param url     the target service url
	 * @param timeout timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	public RestResponse executeGetRequest(String url, int timeout) throws CredentialsRequiredException {
		ApplicationLinkRequestFactory requestFactory = getApplicationLinkRequestFactory();
		return executeRequest(requestFactory.createRequest(Request.MethodType.GET, url), requestFactory, timeout);
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request        the {@link ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @param timeout        timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory,
	                                    int timeout) throws CredentialsRequiredException {
		request.setConnectionTimeout(timeout);
		request.setSoTimeout(timeout);
		return executeRequest(request, requestFactory);
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request        the {@link ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory)
			throws CredentialsRequiredException {
		try {
			RestResponse response = request.execute(new ApplicationLinkResponseHandler<RestResponse>() {
				@Override
				public RestResponse credentialsRequired(Response response) throws ResponseException {
					return new CredentialsRequiredRestResponse(response);
				}

				@Override
				public RestResponse handle(Response response) throws ResponseException {
					if (response.getStatusCode() == HttpStatus.SC_FORBIDDEN || response.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
						return credentialsRequired(response);
					}
					return new RestResponse(response);
				}
			});
			if (response instanceof CredentialsRequiredRestResponse) {
				throw new CredentialsRequiredException(requestFactory, "Unable to authenticate");
			} else {
				return response;
			}
		} catch (ResponseException e) {
			if (e.getCause() instanceof ConnectException) {
				return new RestResponse("Unable to connect to Jenkins. Connection timed out.");
			} else {
				return new RestResponse("Failed to execute request: " + e.getMessage());
			}
		}
	}

	/**
	 * Helper method to get the optional {@link String} element of the given {@link JSONObject} given by the key given
	 *
	 * @param json the {@link JSONObject} to get the {@link String} value from
	 * @param key  the key to get he corresponding value of
	 * @return the {@link String} value, maybe {@code null}
	 */
	private String optJsonString(JSONObject json, String key) {
		if (json.has(key) && !json.isNull(key)) {
			return json.optString(key);
		} else {
			return null;
		}
	}

	/**
	 * Credentials Required specific {@link RestResponse}
	 */
	private class CredentialsRequiredRestResponse extends RestResponse {

		private CredentialsRequiredRestResponse(Response response) {
			super(response);
		}

	}

	/**
	 * Custom Basic Authentication {@link ApplicationLinkRequestFactory}
	 */
	private class BasicAuthApplicationLinkRequestFactory implements ApplicationLinkRequestFactory {

		private final ApplicationLinkRequestFactory delegate;
		private final Map<String, String> configuration;

		public BasicAuthApplicationLinkRequestFactory(ApplicationLinkRequestFactory delegate, Map<String, String> configuration) {
			this.delegate = delegate;
			this.configuration = configuration;
		}

		@Override
		public ApplicationLinkRequest createRequest(Request.MethodType methodType, String url) throws CredentialsRequiredException {
			return delegate.createRequest(methodType, url)
			               .addBasicAuthentication(configuration.get("username"), configuration.get("password"));
		}

		@Override
		public URI getAuthorisationURI(URI callback) {
			return delegate.getAuthorisationURI(callback);
		}

		@Override
		public URI getAuthorisationURI() {
			return delegate.getAuthorisationURI();
		}

	}

}
