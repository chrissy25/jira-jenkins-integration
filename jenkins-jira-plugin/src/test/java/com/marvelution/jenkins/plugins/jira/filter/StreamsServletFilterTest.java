/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.filter;

import com.gargoylesoftware.htmlunit.Page;
import com.marvelution.jenkins.plugins.jira.applinks.BaseActionTest;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for Stream actions
 *
 * @author Mark Rekveld
 * @since 1.3.3
 */
public class StreamsServletFilterTest extends BaseActionTest {

	@Test
	public void testStreamConfig() throws IOException, SAXException {
		JenkinsRule.WebClient webClient = getWebClient();
		Page page = webClient.goTo("rest/activity-stream/1.0/config?local=true", "application/vnd.atl.streams+json");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_OK));
		assertThat(page.getWebResponse().getContentAsString(), is("{\"filters\":[]}"));
	}

	@Test
	public void testStreamFeed() throws IOException, SAXException {
		JenkinsRule.WebClient webClient = getWebClient();
		Page page = webClient.goTo("plugins/servlet/streams?local=true&use-accept-lang=true&maxResults=5", "application/atom+xml");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_OK));
	}

}
