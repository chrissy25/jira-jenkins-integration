/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.utils;

import javax.servlet.http.HttpServletRequest;

import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.marvelution.jenkins.plugins.jira.utils.UriUtils.X_FORWARDED_FOR;
import static com.marvelution.jenkins.plugins.jira.utils.UriUtils.X_FORWARDED_HOST;
import static com.marvelution.jenkins.plugins.jira.utils.UriUtils.X_FORWARDED_PROTO;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link UriUtils}
 *
 * @author Mark Rekveld
 * @since 1.4.10
 */
@RunWith(MockitoJUnitRunner.class)
public class UriUtilsTest {

	@Mock
	private ApplicationLinkStore applicationLinkStore;
	@Mock
	private HttpServletRequest request;

	@Before
	public void setUp() throws Exception {
		when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost:8080/api/json?depth=2"));
	}

	@After
	public void tearDown() throws Exception {
		verify(request, times(1)).getRequestURL();
		verify(request, times(1)).getAttribute(UriUtils.FORWARD_REQUEST_URI);
		verify(request, times(1)).getHeader(X_FORWARDED_PROTO);
		verify(request, times(1)).getHeader(X_FORWARDED_HOST);
		verifyNoMoreInteractions(request);
	}

	@Test
	public void testGetLogicalUri() throws Exception {
		when(applicationLinkStore.getApplicationProxiedUrl()).thenReturn(null);
		assertThat(UriUtils.getLogicalUri(request, applicationLinkStore), is("http://localhost:8080/api/json?depth=2"));
		verify(request, times(1)).getHeader(X_FORWARDED_FOR);
	}

	@Test
	public void testGetLogicalUriWithPathForwarding() throws Exception {
		when(applicationLinkStore.getApplicationProxiedUrl()).thenReturn(null);
		when(request.getAttribute(UriUtils.FORWARD_REQUEST_URI)).thenReturn("/jenkins/api/json");
		assertThat(UriUtils.getLogicalUri(request, applicationLinkStore), is("http://localhost:8080/jenkins/api/json?depth=2"));
		verify(request, times(1)).getHeader(X_FORWARDED_FOR);
	}

	@Test
	public void testGetLogicalUriWithForwarding() throws Exception {
		when(applicationLinkStore.getApplicationProxiedUrl()).thenReturn(null);
		when(request.getHeader(X_FORWARDED_PROTO)).thenReturn("https");
		when(request.getHeader(X_FORWARDED_HOST)).thenReturn("ci.jenkins-ci.org");
		assertThat(UriUtils.getLogicalUri(request, applicationLinkStore), is("https://ci.jenkins-ci.org/api/json?depth=2"));
		verify(request, never()).getHeader(X_FORWARDED_FOR);
	}

	@Test
	public void testGetLogicalUriWithForwardingWithPortChange() throws Exception {
		when(applicationLinkStore.getApplicationProxiedUrl()).thenReturn(null);
		when(request.getHeader(X_FORWARDED_PROTO)).thenReturn("https");
		when(request.getHeader(X_FORWARDED_HOST)).thenReturn("ci.jenkins-ci.org:80");
		assertThat(UriUtils.getLogicalUri(request, applicationLinkStore), is("https://ci.jenkins-ci.org:80/api/json?depth=2"));
		verify(request, never()).getHeader(X_FORWARDED_FOR);
	}

	@Test
	public void testGetLogicalUriWithProxiedUrl() throws Exception {
		when(applicationLinkStore.getApplicationProxiedUrl()).thenReturn("http://localhost:8080/");
		when(applicationLinkStore.getApplicationUrl()).thenReturn("https://ci.jenkins-ci.org/");
		assertThat(UriUtils.getLogicalUri(request, applicationLinkStore), is("https://ci.jenkins-ci.org/api/json?depth=2"));
		verify(request, times(1)).getHeader(X_FORWARDED_FOR);
	}

}
