/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.action;

import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier;
import hudson.model.AbstractProject;
import hudson.model.Descriptor;
import hudson.model.Item;
import hudson.security.AccessDeniedException2;
import hudson.tasks.Publisher;
import hudson.util.DescribableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static com.marvelution.jenkins.plugins.jira.action.JIRABuildNotifierConfigurationAction.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Testcase for {@link JIRABuildNotifierConfigurationAction}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class JIRABuildNotifierConfigurationActionTest {

	@Mock
	private AbstractProject<?, ?> target;
	private DescribableList<Publisher, Descriptor<Publisher>> publishers;
	@Mock
	private StaplerRequest request;
	@Mock
	private StaplerResponse response;
	private StringWriter writer;
	private JIRABuildNotifierConfigurationAction action;

	/**
	 * Setup the tests
	 *
	 * @throws Exception in case of errors
	 */
	@Before
	public void setUp() throws Exception {
		publishers = new DescribableList<Publisher, Descriptor<Publisher>>(target);
		when(target.getPublishersList()).thenReturn(publishers);
		when(target.getName()).thenReturn("test");
		writer = new StringWriter();
		when(response.getWriter()).thenReturn(new PrintWriter(writer));
		action = new JIRABuildNotifierConfigurationAction(target) {
			@Override
			ApplicationLink getApplicationLink(String appId) throws IOException {
				return null;
			}
		};
	}

	/**
	 * Verify that the icon file name is {@code null} so that no action is visible to users
	 */
	@Test
	public void testIconFileName() {
		assertNull(action.getIconFileName());
	}

	/**
	 * Verify that the display name is {@code null} so that no action is visible to users
	 */
	@Test
	public void testDisplayName() {
		assertNull(action.getDisplayName());
	}

	/**
	 * Verify that the urlname is {@code jira}
	 */
	@Test
	public void testUrlName() {
		assertThat(action.getUrlName(), is(URL_NAME));
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test(expected = AccessDeniedException2.class)
	public void testWithNoAdministrationPermission() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(false);
		action.doDynamic(null, request, response);
		verify(request, never()).getMethod();
		verify(request, never()).hasParameter(APP_ID_PARAMETER);
		verify(request, never()).hasParameter(JOB_ID_PARAMETER);
		verify(request, never()).hasParameter(POST_URL_PARAMETER);
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaGET() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn("GET");
		action.doDynamic(null, request, response);
		verify(request).getMethod();
		verify(request, never()).hasParameter(APP_ID_PARAMETER);
		verify(request, never()).hasParameter(JOB_ID_PARAMETER);
		verify(request, never()).hasParameter(POST_URL_PARAMETER);
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaPOSTWithPostUrlAdding() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn(POST);
		when(request.hasParameter(APP_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(JOB_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(POST_URL_PARAMETER)).thenReturn(true);
		when(request.getParameter(POST_URL_PARAMETER)).thenReturn("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		action.doDynamic(null, request, response);
		verify(request).hasParameter(APP_ID_PARAMETER);
		verify(request, never()).hasParameter(JOB_ID_PARAMETER);
		verify(request).hasParameter(POST_URL_PARAMETER);
		verify(request).getParameter(POST_URL_PARAMETER);
		verify(target).getName();
		verify(target).getPublishersList();
		verifyJIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaPOSTWithPostUrlAddingAppLinkFormat() throws Exception {
		action = new JIRABuildNotifierConfigurationAction(target) {
			@Override
			ApplicationLink getApplicationLink(String appId) throws IOException {
				ApplicationLink link = new ApplicationLink();
				link.setId(appId);
				return link;
			}
		};
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn(POST);
		when(request.hasParameter(APP_ID_PARAMETER)).thenReturn(true);
		when(request.hasParameter(JOB_ID_PARAMETER)).thenReturn(true);
		when(request.hasParameter(POST_URL_PARAMETER)).thenReturn(true);
		when(request.getParameter(APP_ID_PARAMETER)).thenReturn("app-link-jira");
		when(request.getParameter(JOB_ID_PARAMETER)).thenReturn("1");
		when(request.getParameter(POST_URL_PARAMETER)).thenReturn("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		action.doDynamic(null, request, response);
		verify(request).hasParameter(APP_ID_PARAMETER);
		verify(request).hasParameter(JOB_ID_PARAMETER);
		verify(request, never()).hasParameter(POST_URL_PARAMETER);
		verify(request, never()).getParameter(POST_URL_PARAMETER);
		verify(target).getName();
		verify(target).getPublishersList();
		verifyJIRABuildNotifier("applink://app-link-jira:1");
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaPOSTWithPostUrlAddingAppLinkFormatMissingAppLink() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn(POST);
		when(request.hasParameter(APP_ID_PARAMETER)).thenReturn(true);
		when(request.hasParameter(JOB_ID_PARAMETER)).thenReturn(true);
		when(request.hasParameter(POST_URL_PARAMETER)).thenReturn(true);
		when(request.getParameter(APP_ID_PARAMETER)).thenReturn("app-link-jira");
		when(request.getParameter(JOB_ID_PARAMETER)).thenReturn("1");
		when(request.getParameter(POST_URL_PARAMETER)).thenReturn("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		action.doDynamic(null, request, response);
		verify(request).hasParameter(APP_ID_PARAMETER);
		verify(request).hasParameter(JOB_ID_PARAMETER);
		verify(request).hasParameter(POST_URL_PARAMETER);
		verify(request).getParameter(POST_URL_PARAMETER);
		verify(target).getName();
		verify(target).getPublishersList();
		verifyJIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
	}

	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaPOSTWithPostUrlReplacing() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn(POST);
		when(request.hasParameter(APP_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(JOB_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(POST_URL_PARAMETER)).thenReturn(true);
		when(request.getParameter(POST_URL_PARAMETER)).thenReturn("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		publishers.add(new JIRABuildNotifier("local"));
		verifyJIRABuildNotifier("local");
		action.doDynamic(null, request, response);
		verify(request).hasParameter(APP_ID_PARAMETER);
		verify(request, never()).hasParameter(JOB_ID_PARAMETER);
		verify(request).hasParameter(POST_URL_PARAMETER);
		verify(request).getParameter(POST_URL_PARAMETER);
		verify(target).getName();
		verify(target).getPublishersList();
		verifyJIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
	}


	/**
	 * Test {@link JIRABuildNotifierConfigurationAction#doDynamic(String, org.kohsuke.stapler.StaplerRequest,
	 * org.kohsuke.stapler.StaplerResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoDynamicViaPOSTWithPostUrlSkipping() throws Exception {
		when(target.hasPermission(Item.CONFIGURE)).thenReturn(true);
		when(request.getMethod()).thenReturn(POST);
		when(request.hasParameter(APP_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(JOB_ID_PARAMETER)).thenReturn(false);
		when(request.hasParameter(POST_URL_PARAMETER)).thenReturn(true);
		when(request.getParameter(POST_URL_PARAMETER)).thenReturn("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		publishers.add(new JIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync"));
		verifyJIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
		action.doDynamic(null, request, response);
		verify(request).hasParameter(APP_ID_PARAMETER);
		verify(request, never()).hasParameter(JOB_ID_PARAMETER);
		verify(request).hasParameter(POST_URL_PARAMETER);
		verify(request).getParameter(POST_URL_PARAMETER);
		verify(target).getName();
		verify(target).getPublishersList();
		verifyJIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync");
	}

	private void verifyJIRABuildNotifier(String postUrl) {
		JIRABuildNotifier notifier = publishers.get(JIRABuildNotifier.class);
		assertThat("There should be a JIRABuildNotifier configured", notifier, notNullValue());
		assertThat("The postUrl of the JIRABuildNotifier should be '" + postUrl + "'", notifier.postUrl, is(postUrl));
	}

}
