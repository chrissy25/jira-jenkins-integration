/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import com.gargoylesoftware.htmlunit.xml.XmlPage;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import hudson.maven.MavenModuleSet;
import hudson.model.FreeStyleProject;
import hudson.model.Hudson;
import hudson.model.Item;
import hudson.security.AuthorizationMatrixProperty;
import hudson.security.Permission;
import hudson.security.ProjectMatrixAuthorizationStrategy;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for the {@link com.marvelution.jenkins.plugins.jira.applinks.EntitiesAction}
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class EntitiesActionTest extends BaseActionTest {

	@Override
	public void setup() throws IOException {
		jenkins.jenkins.setSecurityRealm(jenkins.createDummySecurityRealm());
		ProjectMatrixAuthorizationStrategy authorizationStrategy = new ProjectMatrixAuthorizationStrategy();
		authorizationStrategy.add(Hudson.ADMINISTER, "admin");
		authorizationStrategy.add(Hudson.READ, "user");
		jenkins.jenkins.setAuthorizationStrategy(authorizationStrategy);
		FreeStyleProject freeStyleProject = jenkins.createFreeStyleProject("Freestyle");
		Map<Permission, Set<String>> permissions = Maps.newHashMap();
		permissions.put(Item.CONFIGURE, Sets.newHashSet("admin"));
		freeStyleProject.addProperty(new AuthorizationMatrixProperty(permissions));
		MavenModuleSet mavenProject = jenkins.createMavenProject("Maven");
		permissions = Maps.newHashMap();
		permissions.put(Item.CONFIGURE, Sets.newHashSet("admin", "user"));
		permissions.put(Item.READ, Sets.newHashSet("user"));
		mavenProject.addProperty(new AuthorizationMatrixProperty(permissions));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.EntitiesAction}  for an anonymous user
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testGetEntitiesForAnonymous() throws Exception {
		JenkinsRule.WebClient webClient = jenkins.createWebClient();
		XmlPage page = webClient.goToXml("rest/applinks/1.0/entities");
		assertThat(page.getOwnerDocument().getDocumentElement().getElementsByTagName("entity").getLength(), is(0));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.EntitiesAction}  for an authenticated user
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testGetEntitiesForUser() throws Exception {
		JenkinsRule.WebClient webClient = jenkins.createWebClient();
		webClient.login("user");
		XmlPage page = webClient.goToXml("rest/applinks/1.0/entities");
		assertThat(page.getXmlDocument().getDocumentElement().getElementsByTagName("entity").getLength(), is(1));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.EntitiesAction}  for an admin user
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testGetEntitiesForAdmin() throws Exception {
		JenkinsRule.WebClient webClient = jenkins.createWebClient();
		webClient.login("admin");
		XmlPage page = webClient.goToXml("rest/applinks/1.0/entities");
		assertThat(page.getXmlDocument().getDocumentElement().getElementsByTagName("entity").getLength(), is(2));
	}

}
