/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.gargoylesoftware.htmlunit.xml.XmlPage;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for the {@link com.marvelution.jenkins.plugins.jira.applinks.PermissionAction}
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class PermissionActionTest extends BaseActionTest {

	/**
	 * Verify that anonymous calls result in {@link PermissionAction#NO_PERMISSION}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAnonymousCall() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		XmlPage page = webClient.goToXml("rest/applinks/1.0/permission");
		assertThat(page.getXmlDocument().getDocumentElement().getElementsByTagName("code").item(0).getTextContent(),
		           is(PermissionAction.NO_PERMISSION));
	}

	/**
	 * Verify that anonymous calls result in {@link PermissionAction#ALLOWED}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCall() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		XmlPage page = webClient.goToXml("rest/applinks/1.0/permission");
		assertThat(page.getXmlDocument().getDocumentElement().getElementsByTagName("code").item(0).getTextContent(),
		           is(PermissionAction.ALLOWED));
	}

}
