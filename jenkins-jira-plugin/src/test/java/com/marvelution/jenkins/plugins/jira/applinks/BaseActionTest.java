/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import java.io.IOException;

import hudson.model.Hudson;
import hudson.security.GlobalMatrixAuthorizationStrategy;
import hudson.tasks.Mailer;
import org.junit.Before;
import org.junit.Rule;
import org.jvnet.hudson.test.JenkinsRule;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;

/**
 * Base testcase for actions
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class BaseActionTest {

	@Rule
	public JenkinsRule jenkins = new JenkinsRule();

	/**
	 * Setup the {@link #jenkins} rule with the correct security realm and authentication strategy
	 */
	@Before
	public void setup() throws IOException {
		jenkins.jenkins.setSecurityRealm(jenkins.createDummySecurityRealm());
		GlobalMatrixAuthorizationStrategy authorizationStrategy = new GlobalMatrixAuthorizationStrategy();
		authorizationStrategy.add(Hudson.ADMINISTER, "admin");
		jenkins.jenkins.setAuthorizationStrategy(authorizationStrategy);
		// Reset the Hudson URL, which is unset in JenkinsRule:348, so this can be used by the plugin
		Mailer.descriptor().setHudsonUrl(jenkins.getURL().toExternalForm());
	}

	/**
	 * Get a {@link JenkinsRule.WebClient}
	 *
	 * @return the {@link JenkinsRule.WebClient}
	 */
	protected JenkinsRule.WebClient getWebClient() {
		JenkinsRule.WebClient webClient = jenkins.createWebClient();
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getOptions().setPrintContentOnFailingStatusCode(false);
		webClient.setJavaScriptEnabled(false);
		webClient.setCssErrorHandler(new ErrorHandler() {
			@Override
			public void warning(CSSParseException e) {
			}

			@Override
			public void error(CSSParseException e) {
			}

			@Override
			public void fatalError(CSSParseException e) {
			}
		});
		return webClient;
	}

}
