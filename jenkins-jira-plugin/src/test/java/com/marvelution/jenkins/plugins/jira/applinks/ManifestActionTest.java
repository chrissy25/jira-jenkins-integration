/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import java.io.IOException;
import java.net.URISyntaxException;

import com.marvelution.jenkins.plugins.jira.utils.ApplicationIdUtils;
import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;

import com.gargoylesoftware.htmlunit.xml.XmlPage;
import jenkins.model.Jenkins;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.w3c.dom.Element;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Testcase for the {@link com.marvelution.jenkins.plugins.jira.applinks.ManifestAction}
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ManifestActionTest extends BaseActionTest {

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.ManifestAction}  with an anonymous user
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetManifestAnonymously() throws Exception {
		validateManifest(getWebClient().goToXml("rest/applinks/1.0/manifest"));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.ManifestAction}  with an anonymous user on a secured
	 * Jenkins server
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetManifestSecuredWithoutLogin() throws Exception {
		validateManifest(getWebClient().goToXml("rest/applinks/1.0/manifest"));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.ManifestAction}  with an user on a secured jenkins
	 * servers
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetManifestSecuredWithLogin() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		validateManifest(webClient.goToXml("rest/applinks/1.0/manifest"));
	}

	/**
	 * Validate the Manifest element given
	 *
	 * @param page the {@link XmlPage} that contains the manifest to validate
	 */
	private void validateManifest(XmlPage page) throws IOException, URISyntaxException {
		assertThat(page, notNullValue());
		Element manifest = page.getXmlDocument().getDocumentElement();
		assertThat(manifest.getTagName(), is("manifest"));
		assertThat(getElementContent(manifest, "id"), is(ApplicationIdUtils.getApplicationId(jenkins.getURL())));
		assertThat(getElementContent(manifest, "name"), is(jenkins.jenkins.getDisplayName()));
		assertThat(getElementContent(manifest, "typeId"), is("jenkins"));
		assertThat(getElementContent(manifest, "version"), is(Jenkins.getVersion().toString()));
		assertThat(manifest.getElementsByTagName("buildNumber").getLength(), is(0));
		assertThat(getElementContent(manifest, "applinksVersion"), is(PluginUtils.getAtlassianApplinksVersion()));
		assertThat(getElementContent(manifest, "inboundAuthenticationTypes"),
		           is("com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider"));
		assertThat(getElementContent(manifest, "outboundAuthenticationTypes"),
		           is("com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider"));
		assertThat(getElementContent(manifest, "publicSignup"), is(String.valueOf(jenkins.jenkins.getSecurityRealm()
				.allowsSignup())));
		assertThat(getElementContent(manifest, "url"), is(jenkins.getURL().toString()));
		assertThat(getElementContent(manifest, "iconUrl"), is(jenkins.getURL().toString() +
				PluginUtils.getPluginArifactId() + "/static/images/icon16_jenkins.png"));
	}

	/**
	 * Helper to get the text content of an XML Element
	 *
	 * @param element the main element
	 * @param name    the name of the sub element to get the content of
	 * @return the content
	 */
	private String getElementContent(Element element, String name) {
		return element.getElementsByTagName(name).item(0).getTextContent();
	}

}
