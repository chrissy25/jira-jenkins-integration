/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.scm;

import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import hudson.MarkupText;
import hudson.XmlFile;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.Hudson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Testcase for {@link JIRAChangeLogAnnotator}
 *
 * @author Mark Rekveld
 * @since 1.2.0
 */
public class JIRAChangeLogAnnotatorTest {

	private static ApplicationLinkStore applicationLinkStore;
	private JIRAChangeLogAnnotator annotator;
	private FreeStyleBuild build;

	/**
	 * Load the Application Link Store for the tests
	 *
	 * @throws IOException in case of load errors
	 */
	@BeforeClass
	public static void setupClass() throws IOException {
		URL url = JIRAChangeLogAnnotatorTest.class.getResource("app-links.xml");
		assert url != null;
		XmlFile appLinksXml = new XmlFile(Hudson.XSTREAM, new File(url.getFile()));
		applicationLinkStore = (ApplicationLinkStore) appLinksXml.unmarshal(applicationLinkStore);
	}

	/**
	 * Setup the test variables
	 *
	 * @throws IOException in case of entity link store load errors
	 */
	@Before
	public void setup() throws IOException {
		annotator = spy(new JIRAChangeLogAnnotator());
		doReturn(applicationLinkStore).when(annotator).getApplicationLinkStore();
		FreeStyleProject project = mock(FreeStyleProject.class);
		build = mock(FreeStyleBuild.class);
		when(build.getProject()).thenReturn(project);
		URL url = JIRAChangeLogAnnotatorTest.class.getResource("test-project/jenkins-jira-plugin.xml");
		when(project.getRootDir()).thenReturn(new File(url.getFile()).getParentFile());
	}

	/**
	 * Verify that the Application Link Store is loaded
	 */
	@Test
	public void testHasApplicationLinks() {
		assertThat(applicationLinkStore.get("8835b6b9-5676-3de4-ad59-bbe987416662"), notNullValue());
	}

	/**
	 * Test {@link JIRAChangeLogAnnotator#annotate(hudson.model.AbstractBuild, hudson.scm.ChangeLogSet.Entry,
	 * hudson.MarkupText)} with a single mappable issue in the text
	 */
	@Test
	public void testSingleIssue() {
		MarkupText text = new MarkupText("DUMMY-1!");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>!"));
	}

	/**
	 * Test {@link JIRAChangeLogAnnotator#annotate(hudson.model.AbstractBuild, hudson.scm.ChangeLogSet.Entry,
	 * hudson.MarkupText)} with multiple mappable issues and a single unmappable issue in the text
	 */
	@Test
	public void testMultipleIssues() {
		MarkupText text = new MarkupText("DUMMY-1 Text DUMMY-2,DUMMY-3 DUMMY-4 NOTME-1!");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a> Text " +
				"<a href='http://localhost:2990/jira/browse/DUMMY-2'>DUMMY-2</a>," +
				"<a href='http://localhost:2990/jira/browse/DUMMY-3'>DUMMY-3</a> " +
				"<a href='http://localhost:2990/jira/browse/DUMMY-4'>DUMMY-4</a> NOTME-1!"));
	}

	/**
	 * Test {@link JIRAChangeLogAnnotator#annotate(hudson.model.AbstractBuild, hudson.scm.ChangeLogSet.Entry,
	 * hudson.MarkupText)} with different word boundaries
	 */
	@Test
	public void testWordBoundaries() {
		MarkupText text = new MarkupText("DUMMY-1 Text ");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a> Text "));

		text = new MarkupText("DUMMY-1,comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>,comment"));

		text = new MarkupText("DUMMY-1.comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>.comment"));

		text = new MarkupText("DUMMY-1!comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>!comment"));

		text = new MarkupText("DUMMY-1\tcomment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false),
				is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>\tcomment"));

		text = new MarkupText("DUMMY-1\ncomment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false),
				is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a><br>comment"));
	}

}
