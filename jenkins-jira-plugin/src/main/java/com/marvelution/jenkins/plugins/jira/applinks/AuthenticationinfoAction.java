/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.Manifest;
import hudson.Extension;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handler for the {@code /rest/applinks/1.0/authenticationinfo/*} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class AuthenticationinfoAction extends ApplicationLinksAction {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationinfoAction.class.getName());
	private static final Pattern PATTERN = Pattern.compile("(.*?)/id/(.*?)/url/(.*?$)");
	public static final String ACTION = "authenticationinfo";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doDynamic(StaplerRequest request, StaplerResponse response) throws Exception {
		if (hasPermission(Hudson.getInstance(), Hudson.ADMINISTER)) {
			String path = request.getPathInfo();
			Matcher matcher = PATTERN.matcher(path);
			LOGGER.info(path);
			if (matcher.matches()) {
				String appId = matcher.group(2);
				String remoteUrl = path.substring(matcher.start(3));
				if (!remoteUrl.contains("://") && remoteUrl.contains(":/")) {
					// Don't know how this happens but the a slash was removed to restore this!
					remoteUrl = StringUtils.replaceOnce(remoteUrl, ":/", "://");
				}
				try {
					Manifest remoteManifest = ManifestAction.getRemoteManifest(remoteUrl);
					if (remoteManifest == null || !remoteManifest.getId().equals(appId)) {
						response.sendError(HttpServletResponse.SC_NOT_FOUND, "Given ApplicationId [" + appId + "] doesn't match " +
								"manifest from the remote application at: " + remoteUrl);
					} else {
						response.setStatus(HttpServletResponse.SC_OK);
					}
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Failed to verify the manifest on remote: " + remoteUrl + " with error: "
							+ e.getMessage());
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "Failed to verify manifest");
				}
			} else {
				response.setStatus(HttpServletResponse.SC_OK);
			}
		} else {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
	}

}
