/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.action;

import com.atlassian.oauth.Token;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.google.common.collect.ImmutableList;
import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import com.marvelution.jenkins.plugins.jira.oauth.utils.ConsumerUtils;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.ConsumerInfoStore;
import com.marvelution.jenkins.plugins.jira.store.ServiceProviderTokenStore;
import com.marvelution.jenkins.plugins.jira.utils.UriUtils;
import hudson.Extension;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.server.OAuthServlet;
import org.apache.commons.lang.RandomStringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import java.net.URI;

import static net.oauth.OAuth.*;
import static net.oauth.OAuth.Problems.*;

/**
 * {@link OAuthAction} implemented to allow to request a tokens
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Extension
public class RequestTokenOAuthAction extends OAuthAction {

	private static final String INVALID_CALLBACK_ADVICE =
			"As per OAuth spec version 1.0 Revision A Section 6.1 <http://oauth.net/core/1.0a#auth_step1>, " +
					"the oauth_callback parameter is required and must be either a valid, absolute URI using the http or https scheme, " +
					"or 'oob' if the callback has been established out of band.";
	public static final String URL_NAME = "request-token";
	public static final int TOKEN_SIZE = 32;

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	/**
	 * Handler for the request-token request
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws Exception
	 */
	public void doIndex(StaplerRequest request, StaplerResponse response) throws Exception {
		try {
			OAuthMessage message = OAuthServlet.getMessage(request, UriUtils.getLogicalUri(request, ApplicationLinkStore.getStore()));
			ConsumerInfo consumerInfo = ConsumerInfoStore.getStore().get(message.getConsumerKey());
			if (consumerInfo == null) {
				throw new OAuthProblemException(CONSUMER_KEY_UNKNOWN);
			}
			URI callback = null;
			ServiceProviderToken.Version version = ServiceProviderToken.Version.V_1_0;
			if (message.getParameter(OAUTH_CALLBACK) != null) {
				callback = getCallbackURI(message.getParameter(OAUTH_CALLBACK));
				version = ServiceProviderToken.Version.V_1_0_A;
			}
			JIRAPlugin.getOAuthValidator().validateMessage(message, new OAuthAccessor(ConsumerUtils.toOAuthConsumer(consumerInfo, null)));
			Token token = ServiceProviderTokenStore.getStore().addToken(
					ServiceProviderToken.newRequestToken(RandomStringUtils.randomAlphanumeric(TOKEN_SIZE))
							.tokenSecret(RandomStringUtils.randomAlphanumeric(TOKEN_SIZE))
							.consumer(ConsumerUtils.toConsumer(consumerInfo))
							.callback(callback)
							.version(version)
							.build()
			);
			response.setContentType("text/plain");
			ImmutableList.Builder<OAuth.Parameter> builder = ImmutableList.builder();
			builder.add(new OAuth.Parameter(OAUTH_TOKEN, token.getToken()), new OAuth.Parameter(OAUTH_TOKEN_SECRET,
					token.getTokenSecret()));
			if (callback != null) {
				builder.add(new OAuth.Parameter(OAUTH_CALLBACK_CONFIRMED, Boolean.TRUE.toString()));
			}
			OAuth.formEncode(builder.build(), response.getOutputStream());
		} catch (OAuthProblemException e) {
			OAuthServlet.handleException(response, e, ApplicationLinkStore.getStore().getApplicationUrl());
		}
	}

	/**
	 * Get the actual callback uri
	 *
	 * @param callbackParameter the callback parameter value
	 * @return the callback URI, may be {@code null}
	 * @throws OAuthProblemException
	 */
	private URI getCallbackURI(String callbackParameter) throws OAuthProblemException {
		if ("oob".equals(callbackParameter)) {
			return null;
		}
		try {
			URI callback = new URI(callbackParameter);
			if (!ServiceProviderToken.isValidCallback(callback)) {
				throw new Exception("Invalid callback");
			}
			return callback;
		} catch (Exception e) {
			OAuthProblemException problem = new OAuthProblemException(PARAMETER_REJECTED);
			problem.setParameter(OAUTH_PARAMETERS_REJECTED, OAUTH_CALLBACK);
			problem.setParameter(OAUTH_PROBLEM_ADVICE, INVALID_CALLBACK_ADVICE);
			throw problem;
		}
	}

}
