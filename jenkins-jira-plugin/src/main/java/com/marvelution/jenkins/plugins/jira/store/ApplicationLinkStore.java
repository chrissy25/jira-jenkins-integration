/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.store;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.model.EntityLink;
import com.marvelution.jenkins.plugins.jira.utils.ApplicationIdUtils;

import com.google.common.collect.Maps;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;

/**
 * Store for {@link ApplicationLink}s
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ApplicationLinkStore extends BaseStore {

	private static transient ApplicationLinkStore STORE;
	private final Map<String, ApplicationLink> applinks = Maps.newHashMap();
	private String applicationName;
	private String applicationUrl;
	private String applicationProxiedUrl;
	private String applicationId;

	/**
	 * Private constructor
	 */
	private ApplicationLinkStore() {
		super(Hudson.getInstance().getRootDir());
	}

	/**
	 * Getter for the Store instance
	 *
	 * @return the {@link ApplicationLinkStore}
	 * @throws IOException in case of store load errors
	 */
	public static ApplicationLinkStore getStore() throws IOException {
		if (STORE == null) {
			STORE = new ApplicationLinkStore();
			STORE.load();
		}
		return STORE;
	}

	/**
	 * Getter for the Application Name
	 *
	 * @return the Application Name
	 */
	public String getApplicationName() {
		if (StringUtils.isNotBlank(applicationName)) {
			return applicationName;
		} else {
			return Hudson.getInstance().getDisplayName();
		}
	}

	/**
	 * Getter for the Application URL
	 *
	 * @return the Application URL
	 */
	public String getApplicationUrl() {
		if (StringUtils.isNotBlank(applicationUrl)) {
			return applicationUrl;
		} else {
			return Hudson.getInstance().getRootUrl();
		}
	}

	/**
	 * Getter for the Application Proxied URL
	 *
	 * @return the Application Proxied URL
	 * @since 1.5.4
	 */
	public String getApplicationProxiedUrl() {
		return applicationProxiedUrl;
	}

	/**
	 * Getter for the Application ID
	 *
	 * @return the Application ID
	 */
	public String getApplicationId() {
		if (StringUtils.isBlank(applicationId)) {
			applicationId = ApplicationIdUtils.getApplicationId(URI.create(getApplicationUrl()));
		}
		return applicationId;
	}

	/**
	 * Store the configured name and url
	 *
	 * @param applicationName the Application Name, may be {@code null} or {@code empty}
	 * @param applicationUrl the Application Url, may be {@code null} or {@code empty}
	 * @param applicationProxiedUrl the Application Proxied Url, may be {@code null} or {@code empty}
	 */
	public void configure(String applicationName, String applicationUrl, String applicationProxiedUrl) throws IOException {
		this.applicationName = applicationName;
		if (StringUtils.isNotBlank(applicationUrl) && !applicationUrl.endsWith("/")) {
			applicationUrl += "/";
		}
		this.applicationUrl = !Hudson.getInstance().getRootUrl().equals(applicationUrl) ? applicationUrl : null;
		if (StringUtils.isNotBlank(applicationProxiedUrl)) {
			if (!applicationProxiedUrl.endsWith("/")) {
				applicationProxiedUrl += "/";
			}
			if (!StringUtils.equals(this.applicationUrl, applicationProxiedUrl)) {
				this.applicationProxiedUrl = applicationProxiedUrl;
			}
		} else {
			this.applicationProxiedUrl = null;
		}
		save();
	}

	/**
	 * Getter for all the configured Application Links
	 *
	 * @return all the configured Application Links
	 */
	public Collection<ApplicationLink> getAll() {
		return applinks.values();
	}

	/**
	 * Get an {@link ApplicationLink} by its Id
	 *
	 * @param appId the Application Id
	 * @return the {@link ApplicationLink}, may be {@code null}
	 */
	public ApplicationLink get(String appId) {
		return applinks.get(appId);
	}

	/**
	 * Get an {@link ApplicationLink} by an {@link EntityLink}
	 *
	 * @param entityLink the {@link EntityLink}
	 * @return the {@link ApplicationLink}, may be {@code null}
	 */
	public ApplicationLink get(EntityLink entityLink) {
		if (StringUtils.isNotBlank(entityLink.getApplicationId())) {
			return applinks.get(entityLink.getApplicationId());
		} else {
			return null;
		}
	}

	/**
	 * Add a new {@link ApplicationLink}
	 *
	 * @param appId           the Application Id, and should match the Id in the {@link ApplicationLink}
	 * @param applicationLink the {@link ApplicationLink}
	 * @throws IOException in case of store save errors
	 */
	public synchronized void add(String appId, ApplicationLink applicationLink) throws IOException {
		if (appId.equals(applicationLink.getId())) {
			applinks.put(appId, applicationLink);
			save();
		} else {
			throw new IllegalArgumentException("ApplicationId " + appId + " doesn't match ApplicationLink Id " + applicationLink.getId());
		}
	}

	/**
	 * Remove the {@link ApplicationLink} by its Id
	 *
	 * @param appId the ApplicationLink Id to remove
	 * @throws IOException in case of store save errors
	 */
	public synchronized void remove(String appId) throws IOException {
		if (applinks.containsKey(appId)) {
			applinks.remove(appId);
			save();
		}
	}

}
