/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.notifier;

import com.marvelution.jenkins.plugins.jira.action.JIRABuildNotifierConfigurationAction;
import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Api;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.export.Exported;
import org.kohsuke.stapler.export.ExportedBean;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;

/**
 * Custom {@link Notifier} implementation to notify build completions to JIRA
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExportedBean
public class JIRABuildNotifier extends Notifier {

	@Extension
	public static final JIRADBuildNotifierDescriptor DESCRIPTOR = new JIRADBuildNotifierDescriptor();
	@Exported
	public final String postUrl;

	@DataBoundConstructor
	public JIRABuildNotifier(String postUrl) {
		this.postUrl = URI.create(postUrl).toString();
	}

	@Override
	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE;
	}

	@Override
	public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws
			InterruptedException, IOException {
		WebResource resource = getResource(listener);
		if (resource != null) {
			try {
				ClientResponse response = resource.post(ClientResponse.class);
				if (Response.Status.Family.SUCCESSFUL.equals(response.getClientResponseStatus().getFamily())) {
					listener.getLogger().printf("Successfully triggered JIRA to update builds of this job\n");
				} else {
					listener.error("Unable to trigger JIRA to update builds of this job, JIRA responded with [%d] %s",
							response.getClientResponseStatus().getStatusCode(), response.getClientResponseStatus().getReasonPhrase());
				}
			} catch (Exception e) {
				listener.error("Failed to triggered JIRA to update builds of this job -> %s", e.getMessage());
			}
		}
		return true;
	}

	/**
	 * Getter for the {@link WebResource}
	 *
	 * @param listener the {@link BuildListener}
	 * @return the {@link WebResource}
	 */
	/* package */ WebResource getResource(BuildListener listener) {
		URI uri = URI.create(postUrl);
		if (JIRABuildNotifierConfigurationAction.APPLINK_SCHEME.equals(uri.getScheme())) {
			try {
				ApplicationLink link = ApplicationLinkStore.getStore().get(uri.getHost());
				if (link != null) {
					String jobId = String.valueOf(uri.getPort());
					return new Client().resource(link.getRpcUrl()).path("rest/jenkins/1.0/job").path(jobId).path("sync");
				} else {
					listener.error("No application link found under id: %s", uri.getHost());
					return null;
				}
			} catch (IOException e) {
				listener.error("Failed to load Application Link store: %s", e.getMessage());
				return null;
			}
		} else if (StringUtils.isNotBlank(postUrl)) {
			return new Client().resource(postUrl);
		} else {
			return null;
		}
	}

	/**
	 * getter for the {@link Api} object
	 *
	 * @return the {@link Api} object
	 */
	public final Api getApi() {
		return new Api(this);
	}

	@Override
	public BuildStepDescriptor getDescriptor() {
		return DESCRIPTOR;
	}

	/**
	 * {@link JIRABuildNotifier} descriptor class
	 */
	public static class JIRADBuildNotifierDescriptor extends BuildStepDescriptor<Publisher> {

		@Override
		public boolean isApplicable(Class<? extends AbstractProject> aClass) {
			return true;
		}

		@Override
		public String getDisplayName() {
			return "JIRA Build Notifier";
		}

		/**
		 * Getter for the base of all the help urls
		 *
		 * @return the base help url
		 */
		public String getBaseHelpURL() {
			return "/plugin/" + PluginUtils.getPluginArifactId() + "/static/";
		}

	}

}
