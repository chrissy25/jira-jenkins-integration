/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.PermissionCodeEntity;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import hudson.Extension;
import hudson.model.Hudson;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

/**
 * Handle the {@code /rest/applinks/1.0/permission/*} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class PermissionAction extends ApplicationLinksAction {

	public static final String ALLOWED = "ALLOWED";
	public static final String NO_PERMISSION = "NO_PERMISSION";
	public static final String ACTION = "permission";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doDynamic(StaplerRequest request, StaplerResponse response) throws Exception {
		String code;
		if (hasPermission(Hudson.getInstance(), Hudson.ADMINISTER)) {
			code = ALLOWED;
		} else {
			code = NO_PERMISSION;
		}
		XStreamUtils.writeXmlToResponse(new PermissionCodeEntity(code), response);
	}

}
