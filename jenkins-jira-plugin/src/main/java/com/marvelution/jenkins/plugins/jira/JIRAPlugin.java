/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PrivilegedAction;
import java.security.PublicKey;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.Filter;

import com.marvelution.jenkins.plugins.jira.filter.OAuthFilter;
import com.marvelution.jenkins.plugins.jira.filter.UrlRewriteFilter;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;

import com.atlassian.oauth.util.RSAKeys;
import com.google.common.collect.Lists;
import hudson.Plugin;
import hudson.PluginWrapper;
import hudson.model.Descriptor;
import hudson.model.Hudson;
import hudson.util.PluginServletFilter;
import net.oauth.OAuthValidator;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.springframework.util.ReflectionUtils;

/**
 * The {@link Plugin} implementation for the JIRA Jenkins Plugin.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JIRAPlugin extends Plugin {

	private static final Logger LOGGER = Logger.getLogger(JIRAPlugin.class.getName());
	private static final String OAUTH_PRIVATE_KEY_FILE = "oauth-private-key.pem";
	private static final String OAUTH_PUBLIC_KEY_FILE = "oauth-public-key.pem";
	private static JIRAPlugin self;
	private static Field wrapperField;

	private transient List<Filter> filters;
	private transient PrivateKey privateKey;
	private transient PublicKey publicKey;
	private transient File privateKeyFile;
	private transient File publicKeyFile;

	public JIRAPlugin() {
		filters = Lists.newArrayList(new OAuthFilter(), new UrlRewriteFilter());
		privateKeyFile = new File(Hudson.getInstance().getRootDir(), OAUTH_PRIVATE_KEY_FILE);
		publicKeyFile = new File(Hudson.getInstance().getRootDir(), OAUTH_PUBLIC_KEY_FILE);
	}

	/**
	 * Getter for the plugin short name
	 *
	 * @return the plugin short name
	 */
	public static String getPluginShortName() {
		if (getPluginWrapper() != null) {
			return getPluginWrapper().getShortName();
		} else {
			return PluginUtils.getPluginArifactId();
		}
	}

	/**
	 * Getter for the {@link PluginWrapper}
	 *
	 * @return the {@link PluginWrapper}
	 */
	public static PluginWrapper getPluginWrapper() {
		if (self != null) {
			try {
				return self.getWrapper();
			} catch (NoSuchMethodError e) {
				if (wrapperField == null) {
					wrapperField = AccessController.doPrivileged(new PrivilegedAction<Field>() {
						@Override
						public Field run() {
							return ReflectionUtils.findField(JIRAPlugin.class, null, PluginWrapper.class);
						}
					});
				}
				return AccessController.doPrivileged(new PrivilegedAction<PluginWrapper>() {
					@Override
					public PluginWrapper run() {
						ReflectionUtils.makeAccessible(JIRAPlugin.wrapperField);
						return (PluginWrapper) ReflectionUtils.getField(JIRAPlugin.wrapperField, JIRAPlugin.self);
					}
				});
			}
		} else {
			return null;
		}
	}

	/**
	 * Get a {@link javax.servlet.Filter} that is added by this plugin
	 *
	 * @param filterClass the {@link javax.servlet.Filter} class to get
	 * @return the filter implementation, may be {@code null}
	 * @since 1.4.4
	 */
	public static <F extends Filter> F getFilter(Class<F> filterClass) {
		for (Filter filter : getPlugin().filters) {
			if (filterClass.isInstance(filter)) {
				return filterClass.cast(filter);
			}
		}
		return null;
	}

	/**
	 * Get the {@link net.oauth.OAuthValidator} used by the OAuth filter
	 *
	 * @return get the {@link net.oauth.OAuthValidator}
	 * @since 1.4.0
	 */
	public static OAuthValidator getOAuthValidator() {
		return getFilter(OAuthFilter.class).getOAuthValidator();
	}

	/**
	 * Getter for this Plugin instance
	 *
	 * @return the plugin instance
	 * @since 1.4.0
	 */
	public static JIRAPlugin getPlugin() {
		return self;
	}

	@Override
	public void start() throws Exception {
		super.start();
		load();
		LOGGER.info("Adding the required filters");
		for (Filter filter : filters) {
			PluginServletFilter.addFilter(filter);
		}
		self = this;
	}

	@Override
	public void stop() throws Exception {
		self = null;
		LOGGER.info("Removing the previously added filters");
		for (Filter filter : filters) {
			PluginServletFilter.removeFilter(filter);
		}
		save();
		super.stop();
	}

	@Override
	public void configure(StaplerRequest req, JSONObject json) throws Descriptor.FormException {
		try {
			ApplicationLinkStore.getStore().configure(json.optString("applicationName"), json.optString("applicationUrl"),
			                                          json.optString("proxiedUrl"));
		} catch (IOException e) {
			throw new Descriptor.FormException("Unable to store the Application Name/URL", e, "applicationName");
		}
	}

	@Override
	protected void load() throws IOException {
		try {
			if (privateKeyFile.exists() && publicKeyFile.exists()) {
				privateKey = RSAKeys.fromPemEncodingToPrivateKey(FileUtils.readFileToString(privateKeyFile));
				publicKey = RSAKeys.fromPemEncodingToPublicKey(FileUtils.readFileToString(publicKeyFile));
			} else {
				KeyPair pair = RSAKeys.generateKeyPair();
				privateKey = pair.getPrivate();
				publicKey = pair.getPublic();
				save();
			}
		} catch (Exception e) {
			LOGGER.severe("Failed to load OAuth key pair: " + e.getMessage());
		}
	}

	@Override
	public void save() throws IOException {
		if (privateKey != null && publicKey != null) {
			FileUtils.writeStringToFile(privateKeyFile, RSAKeys.toPemEncoding(privateKey));
			FileUtils.writeStringToFile(publicKeyFile, RSAKeys.toPemEncoding(publicKey));
		}
	}

	/**
	 * Getter for the Application Name used in the Manifest
	 *
	 * @return the Application Name, may be {@code null} or {@code empty}
	 */
	public String getApplicationName() {
		try {
			return ApplicationLinkStore.getStore().getApplicationName();
		} catch (IOException e) {
			return Hudson.getInstance().getDisplayName();
		}
	}

	/**
	 * Getter for the Application URL used in the Manifest
	 *
	 * @return the Application URL, may be {@code null} or {@code empty}
	 */
	public String getApplicationUrl() {
		try {
			return StringUtils.defaultIfEmpty(ApplicationLinkStore.getStore().getApplicationUrl(), Hudson.getInstance().getRootUrl());
		} catch (IOException e) {
			return Hudson.getInstance().getRootUrl();
		}
	}

	/**
	 * Getter for the Application Proxied URL used for OAuth validation
	 *
	 * @return the Application Proxied URL, may be {@code null} or {@code empty}
	 */
	public String getApplicationProxiedUrl() {
		try {
			return StringUtils.defaultIfEmpty(ApplicationLinkStore.getStore().getApplicationProxiedUrl(), null);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Getter for the OAuth {@link java.security.PrivateKey}
	 *
	 * @return the {@link java.security.PrivateKey}
	 * @since 1.4.0
	 */
	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	/**
	 * Getter for the OAuth {@link java.security.PublicKey}
	 *
	 * @return the {@link java.security.PublicKey}
	 * @since 1.4.0
	 */
	public PublicKey getPublicKey() {
		return publicKey;
	}

	/**
	 * Getter for the base of all the help urls
	 *
	 * @return the base help url
	 * @since 1.5.4
	 */
	public String getBaseHelpURL() {
		return "/plugin/" + PluginUtils.getPluginArifactId() + "/static/";
	}

}
