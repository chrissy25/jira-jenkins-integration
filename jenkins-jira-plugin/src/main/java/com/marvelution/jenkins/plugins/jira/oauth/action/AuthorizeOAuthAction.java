/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.action;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.SystemClock;
import com.marvelution.jenkins.plugins.jira.oauth.xstream.PrincipalConverter;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.ServiceProviderTokenStore;
import com.marvelution.jenkins.plugins.jira.utils.UriUtils;
import hudson.Extension;
import hudson.model.Hudson;
import hudson.model.User;
import hudson.security.Permission;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.server.OAuthServlet;
import org.apache.commons.lang.RandomStringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URI;

/**
 * {@link OAuthAction} implemented to allow users to authorize tokens
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
@Extension
public class AuthorizeOAuthAction extends OAuthAction {

	public static final String URL_NAME = "authorize";
	public static final int VERIFIER_LENGTH = 6;
	private final Clock clock = new SystemClock();
	private ServiceProviderToken token;
	private String callback;

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	public ServiceProviderToken getToken() {
		return token;
	}

	public String getCallback() {
		return callback;
	}

	public String getApplicationName() throws IOException {
		return ApplicationLinkStore.getStore().getApplicationName();
	}

	public String getUsername() {
		return User.current().getId();
	}

	/**
	 * Handler for the authorize request
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws java.io.IOException
	 * @throws javax.servlet.ServletException
	 */
	public void doIndex(StaplerRequest request, StaplerResponse response) throws IOException, ServletException {
		checkPermission(Hudson.getInstance(), Permission.READ);
		try {
			OAuthMessage requestMessage = OAuthServlet.getMessage(request, UriUtils.getLogicalUri(request, ApplicationLinkStore.getStore()));
			requestMessage.requireParameters(OAuth.OAUTH_TOKEN);
			token = ServiceProviderTokenStore.getStore().getToken(requestMessage.getToken());
			if (token == null || token.isAccessToken()) {
				throw new OAuthProblemException(OAuth.Problems.TOKEN_REJECTED);
			} else if (token.getAuthorization() == ServiceProviderToken.Authorization.AUTHORIZED || token.getAuthorization() ==
					ServiceProviderToken.Authorization.DENIED) {
				throw new OAuthProblemException(OAuth.Problems.TOKEN_USED);
			} else if (token.hasExpired(clock)) {
				throw new OAuthProblemException(OAuth.Problems.TOKEN_EXPIRED);
			}
			if ("POST".equals(request.getMethod())) {
				ServiceProviderToken newToken;
				if (request.hasParameter("approve")) {
					String verifier = RandomStringUtils.randomAlphanumeric(VERIFIER_LENGTH);
					newToken = token.authorize(new PrincipalConverter.UserPrincipal(User.current()), verifier);
				} else if (request.hasParameter("deny")) {
					newToken = token.deny(new PrincipalConverter.UserPrincipal(User.current()));
				} else {
					handleGet(request, response);
					return;
				}
				redirectBack(request, response, ServiceProviderTokenStore.getStore().addToken(newToken));
			} else {
				handleGet(request, response);
			}
		} catch (OAuthProblemException e) {
			OAuthServlet.handleException(response, e, ApplicationLinkStore.getStore().getApplicationUrl());
		}
	}

	/**
	 * Handler for the authorize get request
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws IOException
	 * @throws javax.servlet.ServletException
	 */
	private void handleGet(StaplerRequest request, StaplerResponse response) throws ServletException, IOException {
		if (request.hasParameter(OAuth.OAUTH_CALLBACK)) {
			callback = request.getParameter(OAuth.OAUTH_CALLBACK);
		}
		request.getView(this, "authorize.jelly").forward(request, response);
	}

	/**
	 * Handler for the redirect action
	 *
	 * @param request  the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @param token    the {@link com.atlassian.oauth.serviceprovider.ServiceProviderToken}
	 * @throws IOException
	 */
	private void redirectBack(StaplerRequest request, StaplerResponse response, ServiceProviderToken token) throws IOException,
			ServletException {
		URI callback = null;
		if (token.getVersion() == ServiceProviderToken.Version.V_1_0_A && token.getCallback() != null) {
			callback = token.getCallback();
		} else if (token.getVersion() == ServiceProviderToken.Version.V_1_0 && request.hasParameter(OAuth.OAUTH_CALLBACK)) {
			callback = URI.create(request.getParameter(OAuth.OAUTH_CALLBACK));
		}
		if (callback != null) {
			String newCallback = OAuth.addParameters(callback.toString(), OAuth.OAUTH_TOKEN, token.getToken());
			if (token.getVersion() == ServiceProviderToken.Version.V_1_0_A) {
				newCallback = OAuth.addParameters(newCallback, OAuth.OAUTH_VERIFIER, token.getAuthorization() == ServiceProviderToken
						.Authorization.AUTHORIZED ? token.getVerifier() : "denied");
			}
			response.sendRedirect(newCallback);
		} else {
			String view;
			if (token.getAuthorization() == ServiceProviderToken.Authorization.AUTHORIZED) {
				view = "approved-authorization.jelly";
			} else {
				view = "denied-authorization.jelly";
			}
			request.getView(this, view).forward(request, response);
		}
	}

}
