/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.EntityLink;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.EntityLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import hudson.model.Item;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletResponse;

/**
 * Handle the {@code /rest/applinks/1.0/entitylink/*} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class EntityLinkAction extends ApplicationLinksAction {

	public static final String ACTION = "entitylink";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doDynamic(StaplerRequest request, StaplerResponse response) throws Exception {
		String pathInfo = StringUtils.stripEnd(request.getPathInfo(), "/");
		String jobName = pathInfo.substring(pathInfo.lastIndexOf("/") + 1);
		final String encoding = request.getCharacterEncoding();
		jobName = new String(encoding == null ? jobName.getBytes() : jobName.getBytes(encoding), "UTF-8");
		AbstractProject target = (AbstractProject) Hudson.getInstance().getItem(jobName);
		if (target != null) {
			if (hasPermission(target, Item.CONFIGURE)) {
				EntityLinkStore store = EntityLinkStore.getStore(target);
				if ("PUT".equals(request.getMethod())) {
					EntityLink entityLink = XStreamUtils.getEntityFromRequest(request.getInputStream(), EntityLink.class);
					if (ApplicationLinkStore.getStore().get(entityLink.getApplicationId()) != null) {
						store.add(entityLink);
						response.setStatus(HttpServletResponse.SC_CREATED);
					} else {
						response.sendError(HttpServletResponse.SC_NOT_FOUND, "Unknown Application with Id: " + entityLink
								.getApplicationId());
					}
				} else if ("DELETE".equals(request.getMethod())) {
					String typeId = request.getParameter("typeId");
					String key = request.getParameter("key");
					String applicationId = request.getParameter("applicationId");
					store.remove(typeId, key, applicationId);
					response.setStatus(HttpServletResponse.SC_OK);
				}
			} else {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Unknown job with name: " + jobName);
		}
	}

}
