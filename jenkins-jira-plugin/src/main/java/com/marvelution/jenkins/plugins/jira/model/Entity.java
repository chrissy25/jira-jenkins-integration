/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.model;

import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;

/**
 * Application Link Entity Model
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@XStreamAlias("entity")
public class Entity {

	@XStreamAlias("iconUrl")
	@XStreamAsAttribute
	public final String iconUrl;
	@XStreamAlias("typeId")
	@XStreamAsAttribute
	public final String typeId;
	@XStreamAlias("name")
	@XStreamAsAttribute
	public final String name;
	@XStreamAlias("key")
	@XStreamAsAttribute
	public final String key;

	/**
	 * Private constructor
	 *
	 * @param typeId  the typeId
	 * @param name    the name
	 * @param key     the key
	 * @param iconUrl the iconUrl
	 */
	private Entity(String typeId, String name, String key, String iconUrl) {
		this.typeId = typeId;
		this.name = name;
		this.key = key;
		this.iconUrl = iconUrl;
	}

	/**
	 * Create a {@link Entity} from the given {@link AbstractProject}
	 *
	 * @param abstractProject the {@link AbstractProject} to create the {@link Entity}
	 * @return teh {@link Entity}
	 */
	public static Entity create(AbstractProject abstractProject) {
		String typeId;
		try {
			Class.forName("jenkins.model.Jenkins");
			typeId = "jenkins.job";
		} catch (ClassNotFoundException e) {
			typeId = "hudson.job";
		}
		return new Entity(typeId, abstractProject.getDisplayName(), abstractProject.getName(),
				String.format("%s/%s/static/images/icon16_%s.png", StringUtils.stripEnd(Hudson.getInstance().getRootUrl(), "/"),
						PluginUtils.getPluginArifactId(), typeId));
	}

}
