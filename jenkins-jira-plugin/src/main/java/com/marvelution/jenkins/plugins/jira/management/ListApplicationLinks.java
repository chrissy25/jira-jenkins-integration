/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.management;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.marvelution.jenkins.plugins.jira.applinks.ManifestAction;
import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import com.marvelution.jenkins.plugins.jira.model.Manifest;
import com.marvelution.jenkins.plugins.jira.oauth.action.ConsumerInfoOAuthAction;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.ConsumerInfoStore;
import com.sun.jersey.api.uri.UriBuilderImpl;
import hudson.Extension;
import hudson.model.ManagementLink;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import org.kohsuke.stapler.bind.JavaScriptMethod;

import javax.servlet.ServletException;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@link hudson.model.ManagementLink} extension to manage application links
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
@Extension(ordinal = Integer.MAX_VALUE - 500)
public class ListApplicationLinks extends ManagementLink {

	public static final String URL_NAME = "listApplicationLinks";
	public static final String INVALID_RECIPROCAL_VIEW = "invalid-reciprocal.jelly";
	public static final String RECIPROCAL_VIEW = "create-reciprocal.jelly";
	public static final String LIST_View = "list.jelly";
	public static final String STATUS_RECIPROCAL_FAILURE = "failed.creation.reciprocal";
	public static final String STATUS_RECIPROCAL_SUCCESS = "completed.creation.reciprocal";
	public static final String CREATION_RESULT_ERROR_KEY = "applinkCreationResultError";
	public static final String APPLINK_ORIGINAL_CREATED_ID = "applinkOriginalCreatedId";
	public static final String APPLINK_CREATION_STATUS_LOG = "applinkCreationStatusLog";
	public static final String SHARED_USERBASE = "sharedUserbase";
	private static final Logger LOGGER = Logger.getLogger(ListApplicationLinks.class.getName());
	private Gson gson;
	private Manifest ownManifest;
	private Manifest remoteManifest;
	private JSONArray applinkCreationStatusLog;
	private String applinkStartingUrl;
	private String applinkOriginalCreatedId;
	private boolean sharedUserbase;

	public ListApplicationLinks() {
		gson = new GsonBuilder().create();
	}

	@Override
	public String getIconFileName() {
		return "network.png";
	}

	@Override
	public String getDisplayName() {
		return Messages.application_links_link();
	}

	@Override
	public String getDescription() {
		return Messages.application_links_description();
	}

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	public void doIndex(StaplerRequest request, StaplerResponse response) throws IOException, ServletException {
		String viewName = INVALID_RECIPROCAL_VIEW;
		applinkOriginalCreatedId = getParameterValue(request, APPLINK_ORIGINAL_CREATED_ID);
		applinkStartingUrl = getParameterValue(request, "applinkStartingUrl");
		if (StringUtils.isNotBlank(applinkOriginalCreatedId) && StringUtils.isNotBlank(applinkStartingUrl)) {
			applinkCreationStatusLog = (JSONArray) JSONSerializer.toJSON(getParameterValue(request, APPLINK_CREATION_STATUS_LOG));
			String applinkStartingType = getParameterValue(request, "applinkStartingType");
			sharedUserbase = Boolean.parseBoolean(getParameterValue(request, SHARED_USERBASE));
			try {
				remoteManifest = ManifestAction.getRemoteManifest(applinkStartingUrl);
				if (remoteManifest != null && applinkStartingType.equals(remoteManifest.getTypeId())) {
					viewName = RECIPROCAL_VIEW;
				}
				ownManifest = ManifestAction.getOwnManifest(request);
			} catch (Exception e) {
				LOGGER.log(Level.SEVERE, "Failed to retrieve manifest of remove application: " + e.getMessage(), e);
			}
		} else {
			viewName = LIST_View;
		}
		request.getView(this, viewName).forward(request, response);
	}

	/**
	 * JavaScript method called when the reciprocal link creation is accepted.
	 * It will also save the link information.
	 *
	 * @return the redirectUrl
	 */
	@JavaScriptMethod
	public JSONObject saveApplicationLink() throws IOException {
		ApplicationLink applink = new ApplicationLink();
		applink.setId(remoteManifest.getId());
		applink.setTypeId(remoteManifest.getTypeId());
		applink.setName(remoteManifest.getName());
		applink.setRpcUrl(remoteManifest.getUrl());
		applink.setDisplayUrl(remoteManifest.getUrl());
		applink.setIconUrl(remoteManifest.getIconUrl());
		applink.setSharedUsers(sharedUserbase);
		loadAndStoreConsumerInfo(applink);
		ApplicationLinkStore.getStore().add(remoteManifest.getId(), applink);
		updateStatusLog(true);
		return new JSONObject().element("redirectUrl", buildRedirectUrl().build().toASCIIString());
	}

	/**
	 * JavaScript method called when the reciprocal link creation is canceled
	 *
	 * @return the redirectUrl
	 */
	@JavaScriptMethod
	public JSONObject cancelApplicationLink() {
		updateStatusLog(false);
		return new JSONObject().element("redirectUrl", buildRedirectUrl().queryParam(CREATION_RESULT_ERROR_KEY,
				gson.toJson(Messages.reciprocal_link_canceled())).build().toASCIIString());
	}

	public Manifest getOwnManifest() {
		return ownManifest;
	}

	public Manifest getRemoteManifest() {
		return remoteManifest;
	}

	public Collection<ApplicationLink> getApplicationLinks() throws IOException {
		return ImmutableList.copyOf(ApplicationLinkStore.getStore().getAll());
	}

	/**
	 * Check the given {@link ApplicationLink} for its {@link ApplicationLink.State} and update the application name if needed
	 *
	 * @param link the {@link ApplicationLink} to check
	 * @return the updated {@link ApplicationLink link} with the its state and updated name
	 */
	public ApplicationLink checkApplicationLink(ApplicationLink link) {
		LOGGER.info("Checking application link: " + link.getName() + " [" + link.getId() + "] at " + link.getRpcUrl());
		try {
			Manifest manifest = ManifestAction.getRemoteManifest(link.getRpcUrl());
			if (link.getId().equals(manifest.getId())) {
				link.setName(manifest.getName());
				link.setState(ApplicationLink.State.ONLINE);
				loadAndStoreConsumerInfo(link);
			} else {
				LOGGER.log(Level.SEVERE, "Application link: " + link.getId() + " at " + link.getRpcUrl() + " seems to be moved.");
				link.setState(ApplicationLink.State.MOVED);
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Failed to update application link data for link: " + link.getId() + " at " + link.getRpcUrl(), e);
			link.setState(ApplicationLink.State.OFFLINE);
		}
		return link;
	}

	/**
	 * Load the {@link ConsumerInfo} from the given {@link ApplicationLink} storing it in the {@link ConsumerInfoStore} and adding a
	 * reference to the application link given
	 *
	 * @param link the {@link ApplicationLink} to load the {@link ConsumerInfo} from
	 */
	private void loadAndStoreConsumerInfo(ApplicationLink link) {
		try {
			if (link.getConsumerKey() == null ||  ConsumerInfoStore.getStore().get(link.getConsumerKey()) == null) {
				ConsumerInfo remoteConsumerInfo = ConsumerInfoOAuthAction.getRemoteConsumerInfo(link.getRpcUrl());
				remoteConsumerInfo.setName(link.getName()); // Make sure the names match!
				ConsumerInfoStore.getStore().add(remoteConsumerInfo);
				link.setConsumerKey(remoteConsumerInfo.getKey());
			} else {
				ConsumerInfoStore.getStore().get(link.getConsumerKey()).setName(link.getName());
			}
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Unable to update consumer information of application link: " + link.getName());
		}
	}

	/**
	 * Build the redirect URI
	 *
	 * @return the {@link javax.ws.rs.core.UriBuilder}
	 */
	private UriBuilder buildRedirectUrl() {
		// Using UriBuilder.fromUri() has issues on Glassfish!
		UriBuilder builder = new UriBuilderImpl().uri(URI.create(applinkStartingUrl)).path("/plugins/servlet/applinks/").path(URL_NAME);
		builder.queryParam(SHARED_USERBASE, gson.toJson(sharedUserbase)).queryParam(APPLINK_ORIGINAL_CREATED_ID,
				gson.toJson(applinkOriginalCreatedId));
		builder.queryParam(APPLINK_CREATION_STATUS_LOG, gson.toJson(applinkCreationStatusLog.toString()));
		return builder;
	}

	/**
	 * Update the {@link #applinkCreationStatusLog}
	 *
	 * @param successful flag if the reciprocal creation was successful or not
	 */
	private void updateStatusLog(boolean successful) {
		applinkCreationStatusLog.add(new JSONObject().element("status", successful ? STATUS_RECIPROCAL_SUCCESS :
				STATUS_RECIPROCAL_FAILURE));
	}

	/**
	 * Helper to get the parameter value
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to get the parameter value from
	 * @param name    the parameter name
	 * @return the parameter value
	 */
	private String getParameterValue(StaplerRequest request, String name) {
		if (request.getParameterMap().containsKey(name)) {
			String parameter = request.getParameter(name);
			return gson.fromJson(StringUtils.isNotBlank(parameter) ? parameter : "", JsonElement.class).getAsString();
		} else {
			return null;
		}
	}

}
