/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.action;

import hudson.ExtensionList;
import hudson.ExtensionPoint;
import hudson.model.Action;
import hudson.model.Hudson;
import hudson.security.AccessControlled;
import hudson.security.AccessDeniedException2;
import hudson.security.Permission;
import org.acegisecurity.Authentication;

/**
 * Jenkins ExtensionPoint for OAuth Actions
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class OAuthAction implements Action, ExtensionPoint {

	/**
	 * Getter for all Implementations of the {@link OAuthAction} {@link hudson.ExtensionPoint}s
	 *
	 * @return {@link hudson.ExtensionList}
	 */
	public static ExtensionList<OAuthAction> all() {
		return Hudson.getInstance().getExtensionList(OAuthAction.class);
	}

	/**
	 * Check if the current user has the given {@link Permission} on the {@link AccessControlled} object given
	 *
	 * @param ac         the {@link AccessControlled} object
	 * @param permission the {@link Permission}
	 * @param <AC>
	 * @return {@code true} if the user has the {@link Permission}, {@code false} otherwise
	 */
	protected <AC extends AccessControlled> boolean hasPermission(AC ac, Permission permission) {
		Authentication authentication = Hudson.getAuthentication();
		return authentication != Hudson.ANONYMOUS && ac.getACL().hasPermission(authentication, permission);
	}

	/**
	 * Check if the current user has the given {@link Permission} on the {@link AccessControlled} object given
	 *
	 * @param ac         the {@link AccessControlled} object
	 * @param permission the {@link Permission}
	 * @param <AC>
	 * @throws AccessDeniedException2 if the user does not have the {@link Permission}
	 */
	protected <AC extends AccessControlled> void checkPermission(AC ac, Permission permission) {
		if (!hasPermission(ac, permission)) {
			throw new AccessDeniedException2(Hudson.getAuthentication(), permission);
		}
	}

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

}
