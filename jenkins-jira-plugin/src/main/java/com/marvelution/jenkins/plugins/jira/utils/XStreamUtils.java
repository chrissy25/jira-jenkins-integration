/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.utils;

import com.marvelution.jenkins.plugins.jira.model.*;
import com.thoughtworks.xstream.XStream;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Helper for {@link XStream}
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class XStreamUtils {

	public static final XStream XSTREAM = new XStream();

	static {
		XSTREAM.autodetectAnnotations(true);
		XSTREAM.ignoreUnknownElements();
		XSTREAM.processAnnotations(new Class[] { Manifest.class, ApplicationLink.class, ApplicationLink.Link.class,
				Entities.class, Entity.class, EntityLink.class, PermissionCodeEntity.class, ConsumerInfo.class
		});
	}

	/**
	 * Write the given object as XML to the {@link HttpServletResponse}
	 *
	 * @param object the object to write
	 * @param response the {@link HttpServletResponse}
	 * @throws IOException in case of write errors
	 */
	public static void writeXmlToResponse(Object object, HttpServletResponse response) throws IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/xml");
		response.setCharacterEncoding("UTF-8");
		XSTREAM.toXML(object, response.getWriter());
	}

	/**
	 * Get an object from the {@link InputStream}
	 *
	 * @param input the {@link InputStream}
	 * @param type the Class type
	 * @param <T> the type generic
	 * @return the Object
	 * @throws IOException in case of read errors
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getEntityFromRequest(InputStream input, Class<T> type) throws IOException {
		ClassLoader loader = null;
		try {
			loader = XSTREAM.getClassLoader();
			XSTREAM.setClassLoader(type.getClassLoader());
			return (T) XSTREAM.fromXML(input);
		} finally {
			if (loader != null) {
				XSTREAM.setClassLoader(loader);
			}
		}
	}

}
