/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.action;

import com.atlassian.oauth.util.RSAKeys;
import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Extension;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * {@link OAuthAction} to handle consumer-info requests
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
@Extension
public class ConsumerInfoOAuthAction extends OAuthAction {

	private static final String URL_NAME = "consumer-info";

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	public void doIndex(StaplerRequest request, StaplerResponse response) throws Exception {
		XStreamUtils.writeXmlToResponse(getOwnConsumerInfo(request), response);
	}

	/**
	 * Get the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} of the local instance
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest}, used to determine the application root url
	 * @return the ConsumerInfo
	 * @throws IOException in case errors
	 */
	public static ConsumerInfo getOwnConsumerInfo(HttpServletRequest request) throws IOException {
		String url = ApplicationLinkStore.getStore().getApplicationUrl();
		String id = ApplicationLinkStore.getStore().getApplicationId();
		ConsumerInfo consumerInfo = new ConsumerInfo();
		try {
			Class.forName("jenkins.model.Jenkins");
			consumerInfo.setKey("jenkins:" + id);
			consumerInfo.setDescription("Jenkins at " + url);
		} catch (ClassNotFoundException e) {
			consumerInfo.setKey("jenkins:" + id);
			consumerInfo.setDescription("Hudson at " + url);
		}
		String name = ApplicationLinkStore.getStore().getApplicationName();
		if (StringUtils.isBlank(name)) {
			name = Hudson.getInstance().getDisplayName();
		}
		consumerInfo.setName(name);
		consumerInfo.setPublicKey(RSAKeys.toPemEncoding(JIRAPlugin.getPlugin().getPublicKey()));
		return consumerInfo;
	}

	/**
	 * Get the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo} of the remote system
	 *
	 * @param remoteUrl the base URL of the remote Application Link system
	 * @return the {@link com.marvelution.jenkins.plugins.jira.model.ConsumerInfo}
	 */
	public static ConsumerInfo getRemoteConsumerInfo(String remoteUrl) throws IOException {
		WebResource webResource = new Client().resource(remoteUrl).path("/plugins/servlet/oauth/").path(URL_NAME);
		ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML_TYPE).get(ClientResponse.class);
		return XStreamUtils.getEntityFromRequest(clientResponse.getEntityInputStream(), ConsumerInfo.class);
	}

}
