/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.model;

import com.google.common.base.Objects;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * Application Link Object
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@XStreamAlias("applicationLink")
public class ApplicationLink {

	@XStreamAlias("link")
	private Link link;
	@XStreamAlias("id")
	private String id;
	@XStreamAlias("typeId")
	private String typeId;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("displayUrl")
	private String displayUrl;
	@XStreamAlias("rpcUrl")
	private String rpcUrl;
	@XStreamAlias("iconUrl")
	private String iconUrl;
	@XStreamAlias("isPrimary")
	private Boolean primary;
	@XStreamAlias("isSystem")
	private Boolean system;
	@XStreamAlias("sharedUsers")
	private Boolean sharedUsers;
	@XStreamAlias("consumerKey")
	private String consumerKey;
	@XStreamOmitField
	private State state = State.UNKNOWN;

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayUrl() {
		return displayUrl;
	}

	public void setDisplayUrl(String displayUrl) {
		this.displayUrl = displayUrl;
	}

	public String getRpcUrl() {
		return rpcUrl;
	}

	public void setRpcUrl(String rpcUrl) {
		this.rpcUrl = rpcUrl;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Boolean getPrimary() {
		return primary;
	}

	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}

	public Boolean getSystem() {
		return system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public Boolean getSharedUsers() {
		return sharedUsers;
	}

	public void setSharedUsers(Boolean sharedUsers) {
		this.sharedUsers = sharedUsers;
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("typeId", typeId).add("name", name)
				.add("rpcUrl", rpcUrl).toString();
	}

	@XStreamAlias("link")
	public static class Link {

		@XStreamAlias("rel")
		@XStreamAsAttribute
		private String relation;
		@XStreamAlias(("href"))
		@XStreamAsAttribute
		private String location;

		public String getRelation() {
			return relation;
		}

		public void setRelation(String relation) {
			this.relation = relation;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

	}

	public static enum State {
		UNKNOWN, ONLINE, OFFLINE, MOVED
	}

}
