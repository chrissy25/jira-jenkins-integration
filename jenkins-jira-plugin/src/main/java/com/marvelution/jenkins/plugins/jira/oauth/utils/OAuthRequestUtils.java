/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.utils;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.marvelution.jenkins.plugins.jira.oauth.action.AccessTokenOAuthAction;
import com.marvelution.jenkins.plugins.jira.oauth.action.OAuthRootAction;
import com.marvelution.jenkins.plugins.jira.oauth.action.RequestTokenOAuthAction;
import net.oauth.OAuth;
import net.oauth.server.HttpRequestMessage;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static net.oauth.OAuth.*;

/**
 * OAuth request utils
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public final class OAuthRequestUtils {

	static final Set<String> OAUTH_DATA_REQUEST_PARAMS = ImmutableSet.of(OAUTH_CONSUMER_KEY, OAUTH_TOKEN, OAUTH_SIGNATURE_METHOD,
			OAUTH_SIGNATURE, OAUTH_TIMESTAMP, OAUTH_NONCE);

	/**
	 * Check if the given {@code request} is an OAuth Access request
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to check
	 * @return {@code true} if {@link #is2LOAuthAccessAttempt(javax.servlet.http.HttpServletRequest)} or {@link
	 * #is3LOAuthAccessAttempt(javax.servlet.http.HttpServletRequest)} returns {@code true}
	 * @see #is2LOAuthAccessAttempt(javax.servlet.http.HttpServletRequest)
	 * @see #is3LOAuthAccessAttempt(javax.servlet.http.HttpServletRequest)
	 */
	public static boolean isOAuthAccessAttempt(HttpServletRequest request) {
		return is2LOAuthAccessAttempt(request) || is3LOAuthAccessAttempt(request);
	}

	/**
	 * Check if the given request is a 2 Legged OAuth access request
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to check
	 * @return {@code true} if the request is a 2 Legged OAuth access request
	 */
	public static boolean is2LOAuthAccessAttempt(HttpServletRequest request) {
		final Map<String, String> params = oauthParameters(request);
		// http://oauth.googlecode.com/svn/spec/ext/consumer_request/1.0/drafts/2/spec.html
		// oauth_token: MUST be included with an empty value to indicate this is a two-legged request
		return params.keySet().containsAll(OAUTH_DATA_REQUEST_PARAMS) && "".equals(params.get(OAuth.OAUTH_TOKEN)) &&
				!isRequestTokenRequest(request);
	}

	/**
	 * Check if the given request is a 3 Legged OAuth access request
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to check
	 * @return {@code true} if the request is a 3 Legged OAuth access request
	 */
	public static boolean is3LOAuthAccessAttempt(HttpServletRequest request) {
		final Map<String, String> params = oauthParameters(request);
		// all the oauth request parameters must be present and oauth_token must not be empty
		return params.keySet().containsAll(OAUTH_DATA_REQUEST_PARAMS) && params.containsKey(OAuth.OAUTH_TOKEN) && !"".equals(params.get
				(OAuth.OAUTH_TOKEN)) && !isAccessTokenRequest(request);
	}

	/**
	 * Check if the given {@code request} is an OAuth Request Token request
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to check
	 * @return {@code true} if the request points to the {@link com.marvelution.jenkins.plugins.jira.oauth.action.RequestTokenOAuthAction}
	 * action
	 */
	private static boolean isRequestTokenRequest(HttpServletRequest request) {
		return StringUtils.stripEnd(request.getRequestURI(), "/").endsWith(OAuthRootAction.URL_NAME + "/" + RequestTokenOAuthAction
				.URL_NAME);
	}

	/**
	 * Check if the given {@code request} is an OAuth Access Token request
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest} to check
	 * @return {@code true} if the request points to the {@link com.marvelution.jenkins.plugins.jira.oauth.action.AccessTokenOAuthAction}
	 * action
	 */
	private static boolean isAccessTokenRequest(HttpServletRequest request) {
		return StringUtils.stripEnd(request.getRequestURI(), "/").endsWith(OAuthRootAction.URL_NAME + "/" + AccessTokenOAuthAction
				.URL_NAME);
	}

	/**
	 * Get all the OAuth parameters as Map
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest}
	 * @return the OAuth parameter {@link java.util.Map}
	 */
	private static Map<String, String> oauthParameters(HttpServletRequest request) {
		Map<String, String> params = Maps.newHashMap();
		for (OAuth.Parameter param : HttpRequestMessage.getParameters(request)) {
			params.put(param.getKey(), param.getValue());
		}
		return Collections.unmodifiableMap(params);
	}

}
