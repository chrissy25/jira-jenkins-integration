/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.utils;

import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;

import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;

/**
 * Utils for URL/URI related objects
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class UriUtils {

	public static final String X_FORWARDED = "X-Forwarded-";
	public static final String X_FORWARDED_PROTO = X_FORWARDED + "PROTO";
	public static final String X_FORWARDED_HOST = X_FORWARDED + "HOST";
	public static final String X_FORWARDED_FOR = X_FORWARDED + "FOR";
	/**
	 * The request attribute key that the request dispatcher uses to store the
	 * original URL for a forwarded request.
	 */
	public static final String FORWARD_REQUEST_URI = "javax.servlet.forward.request_uri";

	/**
	 * Get the logical URI for the given {@code request}
	 *
	 * @param request the {@link HttpServletRequest}
	 * @param applicationLinkStore the {@link ApplicationLinkStore} used to lookup possible use of a proxy URL
	 * @return the forwarded URI or {@code null}
	 */
	public static String getLogicalUri(HttpServletRequest request, ApplicationLinkStore applicationLinkStore) {
		URI uri = URI.create(request.getRequestURL().toString());
		String scheme;
		if ((scheme = request.getHeader(X_FORWARDED_PROTO)) == null) {
			scheme = uri.getScheme();
		}
		String authority;
		if ((authority = request.getHeader(X_FORWARDED_HOST)) == null) {
			if ((authority = request.getHeader(X_FORWARDED_FOR)) == null) {
				authority = uri.getAuthority();
			}
		}
		String path;
		if ((path = (String) request.getAttribute(FORWARD_REQUEST_URI)) == null) {
			path = uri.getPath();
		}
		try {
			String logicalUri = new URI(scheme, authority, path, uri.getQuery(), uri.getFragment()).toString();
			String proxiedUrl = applicationLinkStore != null ? applicationLinkStore.getApplicationProxiedUrl() : null;
			if (proxiedUrl != null && logicalUri.startsWith(proxiedUrl)) {
				// The logical URL found starts with the URL that is configures to be proxied. Replace it with the application URL
				logicalUri = applicationLinkStore.getApplicationUrl() + logicalUri.substring(proxiedUrl.length());
			}
			return logicalUri;
		} catch (URISyntaxException e) {
			return null;
		}
	}

}
