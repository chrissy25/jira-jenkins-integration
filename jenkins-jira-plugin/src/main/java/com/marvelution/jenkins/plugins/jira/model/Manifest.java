/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.marvelution.jenkins.plugins.jira.utils.ApplicationIdUtils;
import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.util.List;

/**
 * Application Link Manifest model
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XStreamAlias("manifest")
public class Manifest {

	public static final String AUTH_PACKAGE = "com.atlassian.applinks.api.auth.types.";
	public static final String BASIC_AUTHENTICATION_PROVIDER = AUTH_PACKAGE + "BasicAuthenticationProvider";
	public static final String TWO_LEGGED_OAUTH_AUTHENTICATION_PROVIDER = AUTH_PACKAGE + "TwoLeggedOAuthAuthenticationProvider";

	@XStreamAlias("id")
	private String id;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("typeId")
	private String typeId;
	@XStreamAlias("version")
	private String version;
	@XStreamAlias("buildNumber")
	private String buildNumber;
	@XStreamAlias("applinksVersion")
	private String applinksVersion;
	@XStreamImplicit(itemFieldName = "inboundAuthenticationTypes")
	private List<String> inboundAuthenticationTypes;
	@XStreamImplicit(itemFieldName = "outboundAuthenticationTypes")
	private List<String> outboundAuthenticationTypes;
	@XStreamAlias("publicSignup")
	private Boolean publicSignup;
	@XStreamAlias("url")
	private String url;
	@XStreamAlias("iconUrl")
	private String iconUrl;

	/**
	 * Private constructor
	 *
	 * @param id                  the application id
	 * @param name                the application name
	 * @param typeId              the application type
	 * @param version             current Jenkins version
	 * @param applinksVersion     the Application Version
	 * @param authenticationTypes the authentication type support
	 * @param publicSignup        flag whether account signup is available
	 * @param url                 the URL of the current instance
	 * @param iconUrl             the icon url of the current instance
	 */
	private Manifest(String id, String name, String typeId, String version, String applinksVersion,
	                 List<String> authenticationTypes, Boolean publicSignup, String url, String iconUrl) {
		this.id = id;
		this.name = name;
		this.typeId = typeId;
		this.version = version;
		this.iconUrl = iconUrl;
		this.buildNumber = null;
		this.applinksVersion = applinksVersion;
		inboundAuthenticationTypes = Lists.newArrayList(authenticationTypes);
		outboundAuthenticationTypes = Lists.newArrayList(authenticationTypes);
		this.publicSignup = publicSignup;
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTypeId() {
		return typeId;
	}

	public String getVersion() {
		return version;
	}

	public String getBuildNumber() {
		return buildNumber;
	}

	public String getApplinksVersion() {
		return applinksVersion;
	}

	public List<String> getInboundAuthenticationTypes() {
		return inboundAuthenticationTypes;
	}

	public List<String> getOutboundAuthenticationTypes() {
		return outboundAuthenticationTypes;
	}

	public Boolean getPublicSignup() {
		return publicSignup;
	}

	public String getUrl() {
		return url;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	/**
	 * Get a {@link Manifest}
	 *
	 * @return the {@link Manifest}
	 */
	public static Manifest create(URI url, String version, String name, boolean publicSignup) {
		String typeId;
		try {
			Class.forName("jenkins.model.Jenkins");
			typeId = "jenkins";
		} catch (ClassNotFoundException e) {
			typeId = "hudson";
		}
		String id = ApplicationIdUtils.getApplicationId(url);
		return new Manifest(id, name, typeId, version, PluginUtils.getAtlassianApplinksVersion(),
				ImmutableList.of(BASIC_AUTHENTICATION_PROVIDER, TWO_LEGGED_OAUTH_AUTHENTICATION_PROVIDER), publicSignup,
				url.toASCIIString(), String.format("%s/%s/static/images/icon16_%s.png", StringUtils.stripEnd(url.toASCIIString(), "/"),
				PluginUtils.getPluginArifactId(), typeId)
		);
	}


}
