/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.action;

import hudson.Extension;
import hudson.security.csrf.CrumbExclusion;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * {@link hudson.security.csrf.CrumbExclusion} implementation to disable the Crumb protection for all Build Notifier related URLs
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
@Extension
public class ActionCrumbExclusion extends CrumbExclusion {

	private static final Logger LOGGER = Logger.getLogger(ActionCrumbExclusion.class.getName());

	@Override
	public boolean process(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (request.getRequestURI().endsWith(JIRABuildNotifierConfigurationAction.URL_NAME + "/")) {
			LOGGER.info("Bypassing the crumb protection filter for Build Notifier request");
			chain.doFilter(request, response);
			return true;
		}
		return false;
	}

}
