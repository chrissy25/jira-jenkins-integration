/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.Entities;
import com.marvelution.jenkins.plugins.jira.model.Entity;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import hudson.model.Item;
import hudson.model.TopLevelItem;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

/**
 * Handler for the {@code /rest/applinks/1.0/entities} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class EntitiesAction extends ApplicationLinksAction {

	public static final String ACTION = "entities";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doIndex(StaplerRequest request, StaplerResponse response) throws Exception {
		Entities entities = new Entities();
		for (TopLevelItem project : Hudson.getInstance().getItems()) {
			if (project instanceof AbstractProject) {
				if (hasPermission(project, Item.CONFIGURE)) {
					entities.entities.add(Entity.create((AbstractProject) project));
				}
			}
		}
		XStreamUtils.writeXmlToResponse(entities, response);
	}

}
