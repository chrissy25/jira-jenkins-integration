/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.filter;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.SystemClock;
import com.google.common.collect.Maps;
import com.marvelution.jenkins.plugins.jira.model.ConsumerInfo;
import com.marvelution.jenkins.plugins.jira.oauth.utils.ConsumerUtils;
import com.marvelution.jenkins.plugins.jira.oauth.utils.OAuthRequestUtils;
import com.marvelution.jenkins.plugins.jira.oauth.xstream.PrincipalConverter;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.store.ConsumerInfoStore;
import com.marvelution.jenkins.plugins.jira.store.ServiceProviderTokenStore;
import com.marvelution.jenkins.plugins.jira.utils.UriUtils;
import hudson.model.User;
import hudson.security.ACL;
import net.oauth.*;
import net.oauth.server.OAuthServlet;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.Problems.*;

/**
 * OAuth security {@link Filter}
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class OAuthFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(OAuthFilter.class.getName());
	private static final String FILTERED_ATTRIBUTE_NAME = OAuthFilter.class.getName();
	private OAuthValidator simpleValidator = new SimpleOAuthValidator(SimpleOAuthValidator.DEFAULT_TIMESTAMP_WINDOW, 2.0);
	private final Clock clock = new SystemClock();

	public OAuthValidator getOAuthValidator() {
		return simpleValidator;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		boolean filtered = request.getAttribute(FILTERED_ATTRIBUTE_NAME) != null;
		HttpServletRequest httpRequest;
		if (filtered) {
			httpRequest = (HttpServletRequest) request;
		} else {
			httpRequest = new RequestWrapper((HttpServletRequest) request);
		}
		if (filtered || !OAuthRequestUtils.isOAuthAccessAttempt(httpRequest)) {
			chain.doFilter(httpRequest, response);
		} else {
			request.setAttribute(FILTERED_ATTRIBUTE_NAME, Boolean.TRUE);
			try {
				doFilterInternal(httpRequest, (HttpServletResponse) response, chain);
			} finally {
				request.removeAttribute(FILTERED_ATTRIBUTE_NAME);
			}
		}
	}

	@Override
	public void destroy() {
	}

	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		Authentication authentication = ACL.SYSTEM;
		try {
			OAuthMessage message = OAuthServlet.getMessage(request, UriUtils.getLogicalUri(request, ApplicationLinkStore.getStore()));
			OAuthConsumer consumer;
			if (OAuthRequestUtils.is2LOAuthAccessAttempt(request)) {
				consumer = validate2LOAuthMessage(message);
			} else if (OAuthRequestUtils.is3LOAuthAccessAttempt(request)) {
				ServiceProviderToken token = ServiceProviderTokenStore.getStore().getToken(message.getToken());
				consumer = validate3LOAuthMessage(message, token);
				if (token.getUser() != null) {
					User user;
					if (token.getUser() instanceof PrincipalConverter.UserPrincipal) {
						user = ((PrincipalConverter.UserPrincipal) token.getUser()).getUser();
					} else {
						user = User.get(token.getUser().getName(), false, Maps.newHashMap());
					}
					if (user != null) {
						authentication = user.impersonate();
					} else {
						throw new OAuthProblemException(TOKEN_REJECTED);
					}
				} else {
					throw new OAuthProblemException(TOKEN_REJECTED);
				}
			} else {
				throw new IllegalArgumentException("Unsupported authentication method. Only 2 and 3 Legged OAuth are supported");
			}
			simpleValidator.validateMessage(message, new OAuthAccessor(consumer));
			loginUser(request, response, chain, authentication);
		} catch (OAuthProblemException e) {
			LOGGER.log(Level.SEVERE, "Failed to handle OAuth message: " + e.getMessage(), e);
			OAuthServlet.handleException(response, e, ApplicationLinkStore.getStore().getApplicationUrl());
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Failed to handle OAuth message: " + e.getMessage(), e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Impersonate the given {@code user} and forward to the request to the filter chain.
	 *
	 * @param request  the original request
	 * @param response the original response
	 * @param chain    the filter chain
	 * @param user     the {@link Authentication user} to impersonate
	 * @throws IOException
	 * @throws ServletException
	 */
	private void loginUser(HttpServletRequest request, ServletResponse response, FilterChain chain,
	                       Authentication user) throws IOException, ServletException {
		SecurityContext oldContext = ACL.impersonate(user);
		try {
			LOGGER.info("Impersonating '" + user.getName() + "' to serve OAuth request: " + request.getRequestURL());
			chain.doFilter(request, response);
		} finally {
			// Kill off session if this is an OAuth attempt.
			if (OAuthRequestUtils.isOAuthAccessAttempt(request) && request.getSession(false) != null) {
				request.getSession().invalidate();
			}
			SecurityContextHolder.setContext(oldContext);
		}
	}

	/**
	 * Validate the given {@code message} as 2 Legged OAuth and get the {@link OAuthConsumer}
	 *
	 * @param message the {@link OAuthMessage} to validate
	 * @return the {@link OAuthConsumer}
	 * @throws Exception
	 */
	private OAuthConsumer validate2LOAuthMessage(OAuthMessage message) throws Exception {
		ConsumerInfo consumerInfo = ConsumerInfoStore.getStore().get(message.getConsumerKey());
		if (consumerInfo == null) {
			throw new OAuthProblemException(CONSUMER_KEY_UNKNOWN);
		}
		message.requireParameters(OAUTH_SIGNATURE_METHOD, OAuth.OAUTH_CONSUMER_KEY);
		if (!message.getParameter(OAUTH_SIGNATURE_METHOD).equals(OAuth.RSA_SHA1)) {
			throw new OAuthProblemException(SIGNATURE_METHOD_REJECTED);
		}
		return ConsumerUtils.toOAuthConsumer(consumerInfo, null);
	}

	/**
	 * Validate the given {@code message} as 3 Legged OAuth and get the {@link OAuthConsumer}
	 *
	 * @param message the {@link OAuthMessage} to validate
	 * @return the {@link OAuthConsumer}
	 * @throws Exception
	 */
	private OAuthConsumer validate3LOAuthMessage(OAuthMessage message, ServiceProviderToken token) throws Exception {
		if (token == null || !token.isAccessToken() || !token.getConsumer().getKey().equals(message.getConsumerKey())) {
			throw new OAuthProblemException(TOKEN_REJECTED);
		}
		if (token.hasExpired(clock)) {
			throw new OAuthProblemException(TOKEN_EXPIRED);
		}
		return ConsumerUtils.toOAuthConsumer(token);
	}

	private static class RequestWrapper extends HttpServletRequestWrapper {

		private static final String FORM_CONTENT_TYPE = "application/x-www-form-urlencoded";
		private static final String FORM_CHARSET = "UTF-8";
		private static final String METHOD_POST = "POST";

		private RequestWrapper(HttpServletRequest request) throws IOException {
			super(request);
		}

		@Override
		public ServletInputStream getInputStream() throws IOException {
			if (isFormPost()) {
				return rebuildInputStream();
			} else {
				return super.getInputStream();
			}
		}

		@SuppressWarnings("unchecked")
		private ServletInputStream rebuildInputStream() throws IOException {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			Writer writer = new OutputStreamWriter(bos, FORM_CHARSET);
			Map<String, String[]> form = getParameterMap();
			for (Iterator<String> nameIterator = form.keySet().iterator(); nameIterator.hasNext(); ) {
				String name = nameIterator.next();
				List<String> values = Arrays.asList(form.get(name));
				for (Iterator<String> valueIterator = values.iterator(); valueIterator.hasNext(); ) {
					String value = valueIterator.next();
					writer.write(URLEncoder.encode(name, FORM_CHARSET));
					if (value != null) {
						writer.write('=');
						writer.write(URLEncoder.encode(value, FORM_CHARSET));
						if (valueIterator.hasNext()) {
							writer.write('&');
						}
					}
				}
				if (nameIterator.hasNext()) {
					writer.append('&');
				}
			}
			writer.flush();
			return new DelegatingServletInputStream(new ByteArrayInputStream(bos.toByteArray()));
		}

		private boolean isFormPost() {
			return (getContentType() != null && getContentType().contains(FORM_CONTENT_TYPE) && METHOD_POST.equalsIgnoreCase(getMethod()));
		}

		private static class DelegatingServletInputStream extends ServletInputStream {

			private final InputStream sourceStream;

			public DelegatingServletInputStream(InputStream sourceStream) {
				this.sourceStream = sourceStream;
			}

			public int read() throws IOException {
				return this.sourceStream.read();
			}

			public void close() throws IOException {
				super.close();
				this.sourceStream.close();
			}

		}

	}

}
