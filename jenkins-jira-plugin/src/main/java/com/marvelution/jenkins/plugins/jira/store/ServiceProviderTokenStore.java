/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.store;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.google.common.collect.Maps;
import com.marvelution.jenkins.plugins.jira.oauth.xstream.ConsumerConverter;
import com.marvelution.jenkins.plugins.jira.oauth.xstream.PrincipalConverter;
import com.thoughtworks.xstream.XStream;
import hudson.model.Hudson;

import java.io.IOException;
import java.util.Map;

/**
 * OAuth Token Store
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class ServiceProviderTokenStore extends BaseStore {

	private static transient ServiceProviderTokenStore STORE;
	private Map<String, ServiceProviderToken> tokens = Maps.newHashMap();

	private ServiceProviderTokenStore(XStream xStream) {
		super(Hudson.getInstance().getRootDir(), "applinks-service-provider-tokens.xml", xStream);
	}

	/**
	 * Get the OAuth Token store instance
	 *
	 * @return the store instance
	 */
	public static ServiceProviderTokenStore getStore() throws IOException {
		if (STORE == null) {
			XStream xStream = new XStream();
			xStream.registerConverter(new ConsumerConverter());
			xStream.registerConverter(new PrincipalConverter());
			STORE = new ServiceProviderTokenStore(xStream);
			STORE.load();
		}
		return STORE;
	}

	/**
	 * Get an OAuth Token by its {@code token}
	 *
	 * @param token the token string
	 * @return the OAuth Token, may be {@code null}
	 */
	public ServiceProviderToken getToken(String token) {
		return tokens.get(token);
	}

	/**
	 * Store the given {@code token}
	 *
	 * @param token the {@link com.atlassian.oauth.serviceprovider.ServiceProviderToken} to store
	 * @return the original {@code token}
	 * @throws IOException in case of storage issues
	 */
	public synchronized ServiceProviderToken addToken(ServiceProviderToken token) throws IOException {
		tokens.put(token.getToken(), token);
		save();
		return token;
	}

	/**
	 * Remove the given {@code token}
	 *
	 * @param token the {@link com.atlassian.oauth.serviceprovider.ServiceProviderToken} to remove
	 * @throws IOException in case of storage issues
	 */
	public synchronized void removeToken(String token) throws IOException {
		tokens.remove(token);
		save();
	}

}
